/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "RTree.h"

using std::cerr;
using std::endl;

RTree::TNode::~TNode()
{
  int J,I;

  if(!Leaf)
    for(J=0,I=UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
      if(I&1) delete Child[J];

  UseMask = 0;
  Leaf    = 1;
  Parent  = 0;
}

RTree::RTree(const char *NewName)
  : QData(NewName)
{
  Depth      = 0;
  ItemCount  = 0;
  Root       = 0;

  (Rect &)*this=Rect::EmptyRect();
}

RTree::~RTree()
{
  if(Root) delete Root;
  Depth     = 0;
  ItemCount = 0;
  Root      = 0;
}

int RTree::Add(const Region &R)
{
  TNode *T,*C1,*C2,*P;
  int J,I;

  // R must not be empty
  if(R.Empty()) return(0);

  // Must be a rectangle or a point
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0);

  // For an empty tree, create root
  if(!Root)
  {
    Root = new TNode;
    if(!Root) return(0);

    Root->UseMask = 0x00000001;
    Root->Parent  = 0;
    Root->Leaf    = 1;
    Root->Data[0] = (Rect &)R;
    Depth++;
    (Rect &)*this=(Rect &)*this+(Rect &)R;
    return(++ItemCount);
  }

  // Choose subtree to insert R into
  T=ChooseSubtree((Rect *)&R);
  if(!T) return(0);

  // Child links to insert is initially 0 as we insert into a leaf
  // and do not split
  C1=C2=0;

  // Insert and split as needed
  do
  {
    // Change existing rectangle bounding box due to a split
    if(C1)
    {
      for(J=0,I=T->UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
        if((I&1)&&(T->Child[J]==C1))
        {
          T->Data[J]=BoundingBox(C1);
          break;
        }

      // Haven't found node in its parent - something is weird
      if(!I) cerr<<"RTree::Add(): Error finding node "<<C1<<" in "<<T<<endl;
    }

    // If there is a free spot in the node...
    if(T->UseMask!=(1<<REGS_PER_NODE)-1)
    {
      // Find free spot and insert
      for(J=0,I=T->UseMask;J<REGS_PER_NODE;J++,I>>=1)
        if(!(I&1))
        {
          // Insert data
          T->UseMask |= 1<<J;
          T->Data[J]  = C2? BoundingBox(C2):(Rect &)R;
          T->Child[J] = C2;
          if(C2) C2->Parent=T;

          // Update bounding boxes
          for(;T->Parent;T=T->Parent)
          {
            for(J=0,I=T->Parent->UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
              if((I&1)&&(T->Parent->Child[J]==T))
              {
                T->Parent->Data[J]=BoundingBox(T);
                break;
              }

            // Haven't found node in its parent - something is weird
            if(!I) cerr<<"RTree::Add(): Error finding node "
                       <<T<<" in "<<T->Parent<<endl;
          }

          // Done inserting
          (Rect &)*this=(Rect &)*this+(Rect &)R;
          return(++ItemCount);
        }

      // Can't find spot - something weird occured
      cerr<<"RTree::Add(): Error finding free spot in "<<T<<endl;
      return(0);
    }

    // No free spot - we need to split
    //J=ChooseSplitAxis(T,(Rect *)R);
    //I=ChooseSplitIndex(T,(Rect *)R,J);

    // Create a new node
    P = new TNode;
    P->Leaf     = T->Leaf;
    P->UseMask  = (1<<(REGS_PER_NODE/2+1))-1;
    T->UseMask  = (1<<(REGS_PER_NODE-REGS_PER_NODE/2))-1; 
    P->Data[0]  = C2? BoundingBox(C2):(Rect &)R;
    P->Child[0] = C2;
    if(C2) C2->Parent=P;

    // Copy half of original node contents
    for(J=0;J<REGS_PER_NODE/2;J++)
    {
      I=J+REGS_PER_NODE-REGS_PER_NODE/2;
      P->Data[J+1]=T->Data[I];
      if(!T->Leaf)
      {
        P->Child[J+1]=T->Child[I];
        P->Child[J+1]->Parent=P;
      }
    }

    // We should now insert new node one level up
    C1   = T;
    C2   = P;
    T    = T->Parent;
  }
  while(T);

  // Need to create new root
  T = new TNode;
  T->Leaf      = 0;
  T->Parent    = 0;
  T->UseMask   = 0x00000003;
  T->Data[0]   = BoundingBox(C1);
  T->Data[1]   = BoundingBox(C2);
  T->Child[0]  = C1;
  T->Child[1]  = C2;
  C1->Parent   = T;
  C2->Parent   = T;
  Root=T;
  Depth++;

  (Rect &)*this=(Rect &)*this+(Rect &)R;
  return(++ItemCount);
}

int RTree::Delete(const Region &R)
{
  // @@@ ADD DELETION CODE HERE!
  return(0);
}

int RTree::Items(const Region &R) const
{
  // @@@ ADD STUFF HERE!
  return(-1);
}

int RTree::Cost(const Region &R) const
{   
  // We consider cost of traversing the tree to be proportional to
  // the number of items in the tree.
  return(ItemCount);
}

Region *RTree::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  int I,J;

  // No root - no data
  if(!Root) { Q.Reset();return(0); }

  // Nothing yet
  Q.Reset(Type());

  // Scan root node for data
  for(J=0,I=Root->UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
    if(I&1)
    {
      C->LastDegree = Depth;
      C->LastTNode  = Root;
      C->LastState  = J;
      *OutDegree = Root->Leaf? 0:Depth;
      return(&(Root->Data[J]));
    }

  // Didn't find anything
  Q.Reset();
  return(0);
}

Region *RTree::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  TNode *P;
  int I,J,GoDeeper;

  // Have to do GetFirst() first
  if(Q.Type()!=Type()) return(0);

  // We want to go deeper into the tree, if InDegree allows
  GoDeeper=(C->LastDegree>InDegree)&&!C->LastTNode->Leaf;

  do
  {
    // Scan current node
    if(C->LastDegree>=InDegree)
    {
      // Scan leaves, deep into non-leaves
      if(!GoDeeper) C->LastState++;
      else
      {
        C->LastTNode=C->LastTNode->Child[C->LastState];
        C->LastState=0;
        C->LastDegree--;
      }

      I=C->LastTNode->UseMask>>C->LastState;
      for(;I&&(C->LastState<REGS_PER_NODE);C->LastState++,I>>=1)
        if(I&1)
        {
          *OutDegree=C->LastTNode->Leaf? 0:C->LastDegree;
          return(&(C->LastTNode->Data[C->LastState]));
        }
    }

    //        
    // Out of data in this node, go to the next one
    //

    // No idea of the state yet, but we are going to go higher, not deeper
    C->LastState=-1;
    GoDeeper=0;

    // Go up as required by InDegree
    do
    {
      P=C->LastTNode;
      C->LastTNode=C->LastTNode->Parent;
      if(!C->LastTNode) { Q.Reset();return(0); }
      C->LastDegree++;
    }
    while(C->LastDegree<=InDegree);

    // Scan for current node in the parent node
    J=0;
    I=C->LastTNode->UseMask;
    for(;I&&(J<REGS_PER_NODE);J++,I>>=1)
      if((I&1)&&(C->LastTNode->Child[J]==P)) { C->LastState=J;break; }
  }
  while(C->LastState>=0);

  // Haven't found node in its parent - something is weird
  cerr<<"RTree::GetNext(): Error finding node "
      <<P<<" in "<<C->LastTNode<<endl;
  Q.Reset();
  return(0);
}

RTree::TNode *RTree::ChooseSubtree(const Rect *R)
{
  double D,MinOver,MinArea;
  int I,J,CheckArea;
  TNode *P;

  for(P=Root;P&&!P->Leaf;P=P->Child[J])
  {
    MinOver=MAXFLOAT;
    MinArea=MAXFLOAT;
 
    // Find the most fitting rectangle to insert to
    for(I=0,J=-1;I<REGS_PER_NODE-1;I++)
      if(P->UseMask&(1<<I))
      {
        // If children are leaves, we minimize the amount of overlap
        // Otherwise, we minimize the enlargement area
        if(!P->Child[I]->Leaf) CheckArea=1;
        else
        {
          CheckArea=0;
          D=ComputeOver(P->Data[I],*R);
          if(D<MinOver) { J=I;MinOver=D; }
          else if(D==MinOver)
               {
                 // For same overlaps, minimize enlargement area
                 MinArea=ComputeArea(P->Data[J],*R);
                 CheckArea=1;
               }
        }

        // Minimize enlargement area
        if(CheckArea)
        {
          D=ComputeArea(P->Data[I],*R);
          if(D<MinArea) { J=I;MinArea=D; }
        }
      }

    // Couldn't find optimal J - something weird occured
    if(J<0)
    {
      cerr<<"RTree::ChooseSubtree(): Error finding optimal subtree"<<endl;
      return(0);
    }
  }

  // Done (may return 0 if no root!)
  return(P);
}

void RTree::Split(const RTree::TNode *T,const Rect &R) const
{
  const Rect *PP,*PX[REGS_PER_NODE+1],*PY[REGS_PER_NODE+1];
  int NX[REGS_PER_NODE+1],NY[REGS_PER_NODE+1];
  int I,J,K,Count;

  // Put rectangle we are adding
  PX[0]=PY[0]=&R;
  NX[0]=NY[0]=-1;
  Count=1;

  // Put existing rectangles
  for(J=0,I=T->UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
    if(I&1)
    {
      PX[Count]=PY[Count]=&(T->Data[J]);
      NX[Count]=NY[Count]=J;
      Count++;
    }

  // Sort by lower bounds on X and Y
  for(K=0;K<Count-1;K++)
    for(J=0;J<Count-1;J++)
      for(I=J+1;I<Count;I++)
      {
        if(PX[J]->X1()>PX[I]->X1()) { PP=PX[J];PX[J]=PX[I];PX[I]=PP; }
        if(PY[J]->Y1()>PY[I]->Y1()) { PP=PY[J];PY[J]=PY[I];PY[I]=PP; }
      }

  // Sort by upper bound on X and Y
  // @@@ WRITE CODE HERE!

  
}

double RTree::ComputeOver(const Rect &R1,const Rect &R2) const
{
  double X,Y;

  X=R1.X1()>R2.X1()? (R2.X2()-R1.X1()):(R1.X2()-R2.X1());
  Y=R1.Y1()>R2.Y1()? (R2.Y2()-R1.Y1()):(R1.Y2()-R2.Y1());
  if((X<=0.0)||(Y<=0.0)) return(0.0);
  return(X*Y);
}

double RTree::ComputeArea(const Rect &R1,const Rect &R2) const
{
  double X,Y;

  X=(R1.X2()>R2.X2()? R1.X2():R2.X2())-(R1.X1()<R2.X1()? R1.X1():R2.X1());
  Y=(R1.Y2()>R2.Y2()? R1.Y2():R2.Y2())-(R1.Y1()<R2.Y1()? R1.Y1():R2.Y1());
  return(X*Y-(R2.X2()-R2.X1())*(R2.Y2()-R2.Y1()));
}

Rect RTree::BoundingBox(const RTree::TNode *T) const
{
  double X1,X2,Y1,Y2;
  int J,I;

  X1=Y1=MAXFLOAT;
  X2=Y2=-MAXFLOAT;

  for(J=0,I=T->UseMask;I&&(J<REGS_PER_NODE);J++,I>>=1)
    if(I&1)
    {
      if(X1>T->Data[J].X1()) X1=T->Data[J].X1();
      if(X2<T->Data[J].X2()) X2=T->Data[J].X2();
      if(Y1>T->Data[J].Y1()) Y1=T->Data[J].Y1();
      if(Y2<T->Data[J].Y2()) Y2=T->Data[J].Y2();
    }

  return(Rect(X1,Y1,X2,Y2));
}
