/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QJoin ****************************************************************
//** This class implements Join query, given two data inputs and a       **
//** selection condition with two free variables.                        **
//*************************************************************************
#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QJoin::ItemsEstimator)(int Min,int Max) = Query::AvgItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QJoin::QJoin(Query *Z1,Query *Z2,WFF *C,Region *V1,Region *V2)
{
  Region *Buf[CoverBufSize];
  int J,N;

  XType   = QU_JOIN;
  XInput1 = Z1;
  XInput2 = Z2;
  XCond   = C;
  XVar1   = V1;
  XVar2   = V2;
  XMono   = C->Mono(V2);
  XAnti   = C->Anti(V2);

  // Find areas of the first dataset covered by the query
  // As only first dataset regions are returned, this will
  // be our query bounding box
  *XVar1=(Rect &)*XInput1;
  *XVar2=(Rect &)*XInput2;
  N=XCond->Eval(XVar1,Buf,CoverBufSize);
  // Compound all areas into a single rectangle
  if(N<0) (Rect &)*this=(Rect &)*XInput1;
  else
    for(J=0,(Rect &)*this=Rect::EmptyRect();J<N;J++)
    {
      (Rect &)*this+=*(Rect *)Buf[J];
      delete Buf[J];
    }
}

Region *QJoin::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();

  // Starting from scratch
  Q.Reset(Type(),2);
  C->R1 = 0;
  C->R2 = 0;

  // Initialize Box
  memcpy(&C->Box,&Rect::EmptyRect(),sizeof(Rect));

  // Return first region
  return(GetNext(Q,0,OutDegree));
}

Region *QJoin::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *Buf[CoverBufSize];
  int J,N,D1,D2;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  // When going up the tree, stop traversing the second dataset
  if(InDegree>0) C->R2=0;

  // Start with degrees of 0
  D1=D2=0;

  do
  {
    // Get next region from the second dataset
    if(C->R2) C->R2=XInput2->GetNext(Q.In(1),D2,&D2);

    // If out of the second dataset...
    if(!C->R2)
    {
      // Get next region from the first dataset
      do
        C->R1=C->R1? XInput1->GetNext(Q.In(0),InDegree,&D1)
                   : XInput1->GetFirst(Q.In(0),&D1);
      while(C->R1&&!C->R1->Intersects(*this));
      // If out of regions in the first dataset, we are done
      if(!C->R1) { Q.Reset();return(0); }
      // Return implicit regions right away
      if(D1>0) { *OutDegree=D1;return(C->R1); }
      // Get first region from the second data set
      C->R2=XInput2->GetFirst(Q.In(1),&D2);
      // Find areas of the second dataset covered by the query  
      *XVar1=*(C->R1);
      *XVar2=(Rect &)*XInput2;
      N=XCond->Eval(XVar2,Buf,CoverBufSize);
      // Compound all areas into a single rectangle
      if(N<0) C->Box=(Rect &)*XInput2;
      else
        for(J=0,C->Box=Rect::EmptyRect();J<N;J++)
        {
          C->Box+=*(Rect *)Buf[J];
          delete Buf[J];
        }
    }

    // If obtained a region from the second dataset...
    // ...and it lies in the area of interest...
    if(C->R2&&C->R2->Intersects(C->Box))
    {
      // If second region is implicit...
      if(D2>0)
      {
        // Always need to traverse when XCond not monotonic 
        if(!XMono) D2=0;
        else
        {
          // Assign variables
          *XVar1=*(C->R1);
          *XVar2=*(C->R2);
          // Evaluate monotonic condition
          if(XCond->Eval()) D2=0;
        }
      }
      else
      {
        // Assign variables
        *XVar1=*(C->R1);
        *XVar2=*(C->R2);
        // Evaluate condition
        if(XCond->Eval()) { *OutDegree=D1;return(C->R1); }
      }
    }
  }
  while(C->R1);

  Q.Reset();
  return(0);
}

int QJoin::Items(const Region &R) const
{
  return(-1);
}

int QJoin::Cost(const Region &R) const
{
  return(-1);
}
