/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "Attrs.h"
#include <math.h>
#include <cstdlib>
#include <cstring>

Attrs AttrBase(ATTRBASESIZE);
// @@@ ATTENTION: THIS STORES ALL ATTRIBUTES FOR NOW!

Attrs::Attrs(int NewHashSize,const char *NewName): QData(NewName)
{
  int J;

  HashSize  = NewHashSize;
  Hash      = new Node *[NewHashSize];
  Head      = 0;
  ItemCount = 0;

  // Clear hash table
  for(J=0;J<NewHashSize;J++) Hash[J]=0;
}

Attrs::~Attrs()
{
  Node *P,*T;

  for(P=Head;P;P=T) { T=P->Next;delete [] (char *)P; }
  if(Hash) delete [] Hash;
  Hash = 0;
  Head = 0;
}

const char *Attrs::Set(const Rect &R,const char *A)
{
  Node *N;
  char *P;
  int H;

  // Allocate space and copy attributes
  H = AttrSize(A);
  P = new char[H+sizeof(Node)];
  N = (Node *)P;
  if(!P) return(0);
  memcpy(P+sizeof(Node),A,H);
  //N->R=R;

  // Find hash code
  H=0;

  if(Hash[H])
  {
    // Hash entry present
    if(Hash[H]->Prev) Hash[H]->Prev->Next=N; else Head=N;
    N->Prev=Hash[H]->Prev;
    N->Next=Hash[H];
    N->End=0;
    Hash[H]->Prev=N;
  }
  else
  {
    // Create new hash entry
    if(Head) Head->Prev=N;
    N->Next=Head;
    N->Prev=0;
    N->End=1;
    Head=N;
  }

  // Success
  Hash[H]=N;
  return((char *)(N+1));
}

const char *Attrs::Get(const Rect &R)
{
  Node *N;
  int H;

  // Find hash code
  H=0;

  // No hash entry - not found
  if(!Hash[H]) return(0);

  // Search
  for(N=Hash[H];N;N=N->End? 0:N->Next)
    if(N->R.Contains(R)&&R.Contains(N->R)) return((char *)(N+1));

  // Nothing found
  return(0);
}

int Attrs::Cost(const Region &R) const
{
  return(ItemCount);
}

Region *Attrs::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)(Q.Data());

  // No list -> no data
  if(!Head) return(0);

  // Return first region
  C->Current=Head;
  OutDegree=0;
  return(&(Head->R));
}

Region *Attrs::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)(Q.Data());

  // No current node - need to do GetFirst()
  if(!C->Current) return(0);

  // Go to the next node
  C->Current=C->Current->Next;
  if(!C->Current) return(0);

  // Return next region
  OutDegree=0;
  return(&(C->Current->R));
}

int Attrs::AttrSize(const char *A) const
{
  int Size,L;

  Size=1;
  do
  {
    L=strlen(A);A+=L+1;Size+=L;
    L=strlen(A);A+=L+1;Size+=L;
  }
  while(*A);

  return(Size);
}

