PRTree D11 = load("DATA1");
PRTree D12 = load("DATA2");
PQTree D21 = D11;
PQTree D22 = D12;
RTree  D31 = D11;
RTree  D32 = D12;

R0 = (-10,-10)-(10,10);

time RSelect(D11+D12,R intersects R0);
time RSelect(D21+D22,R intersects R0);
time RSelect(D31+D32,R intersects R0);

time RSelect(D11,R intersects R0)+RSelect(D12,R intersects R0);
time RSelect(D21,R intersects R0)+RSelect(D22,R intersects R0);
time RSelect(D31,R intersects R0)+RSelect(D32,R intersects R0);

R0 = (0,0);

time Neighbor(D11+D12,R0);
time Neighbor(D21+D22,R0);
time Neighbor(D31+D32,R0);

time Neighbor(Neighbor(D11,R0)+Neighbor(D12,R0),R0);
time Neighbor(Neighbor(D21,R0)+Neighbor(D22,R0),R0);
time Neighbor(Neighbor(D31,R0)+Neighbor(D32,R0),R0);

bye;
