/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef WFF_H
#define WFF_H

#include "Region.h"
#include "Rect.h"
#include <iostream>

#define OP_ILLEGAL    -1
#define OP_FALSE      0   // Logical operators
#define OP_TRUE       1   // ...
#define OP_AND        2
#define OP_OR         3
#define OP_NOT        4
#define OP_INTERSCT   5   // Set operators
#define OP_SUBSETEQ   6   // ...
#define OP_EQUAL      7
#define OP_NEQUAL     8
#define OP_SUBSET     9
#define OP_LT         10  // Comparison operators
#define OP_GT         11  // ...
#define OP_LE         12
#define OP_GE         13
#define OP_EQ         14
#define OP_NE         15
#define OP_ALT        16  // Do not use these when calling constructors!
#define OP_AGT        17  // Use OP_LT..OP_NE instead.
#define OP_ALE        18  // ...
#define OP_AGE        19
#define OP_AEQ        20
#define OP_ANE        21
#define OP_REGEXP     22  // Regular expression match

class WFF: public Rect
{
  public:
    WFF(int Op,WFF *A1,WFF *A2);
    // OP_FALSE,OP_TRUE,OP_AND,OP_OR,OP_NOT
    WFF(int Op,Region *A1,Region *A2);
    // OP_INTERSCT,OP_SUBSETEQ,OP_EQUAL,OP_NEQUAL,OP_SUBSET
    WFF(int Op,Region *A1,Region *A2,double C);
    // OP_LT,OP_GT,OP_LE,OP_GE,OP_EQ,OP_NE (on distances)
    WFF(int Op,Region *A1,Region *A2,Region *A3,Region *A4);
    // OP_LT,OP_GT,OP_LE,OP_GE,OP_EQ,OP_NE (on distances)
    WFF(int Op,Region *A1,const char *N1,double C);
    // OP_LT,OP_GT,OP_LE,OP_GE,OP_EQ,OP_NE (on attributes)
    WFF(int Op,Region *A1,const char *N1,const char *C);
    // OP_LT,OP_GT,OP_LE,OP_GE,OP_EQ,OP_NE,OP_REGEXP (on attributes)
    WFF(int Op,Region *A1,const char *N1,Region *A2,const char *N2);
    // OP_LT,OP_GT,OP_LE,OP_GE,OP_EQ,OP_NE (on attributes)

    int Eval(void) const;
    // Evaluate WFF, returning 1 for TRUE, 0 for FALSE, or -1
    // in case of an error.

    int Eval(const Region *R,Region **Out,int Max=-1) const;
    // Evaluate WFF with respect to R, putting up to Max resulting
    // regions into Out. If Max<0, do not limit number of resulting
    // regions. Return number of regions created or -1 in case of
    // an error.

    int Mono(const Region *R) const;
    // Check if WFF is monotonic with respect to R.

    int Anti(const Region *R) const;
    // Check if WFF is antimonotonic with respect to R.

    void operator delete(void *P);
    // Cascading "delete" operator also deleting all child nodes.

    static WFF *Normalize(WFF *F);
    // Normalize WFF by removing all operators which are neither
    // monotonic nor antimonotonic. This function changes actual
    // WFF nodes and creates new ones with "new" operator. It
    // removes OP_EQUAL,OP_NEQUAL,OP_SUBSET,OP_LE,OP_GE,OP_EQ,OP_NE.
    //
    // This function also tries simplifying the WFF by removing
    // obvious tautologies. Excessive nodes are deleted with "delete"
    // operator.

  private:
    int XOp;

    union
    {
      struct { WFF *A1,*A2; } L;
      struct { Region *A1,*A2; } S;
      struct { Region *A1,*A2,*A3,*A4; double C; } D;
      struct { Region *A1,*A2; const char *N1,*N2,*CS; double CD; } A;
    } Args;

  friend std::ostream &operator <<(std::ostream &Out,const WFF &F);
};

std::ostream &operator <<(std::ostream &Out,const WFF &F);

#endif /* WFF_H */
