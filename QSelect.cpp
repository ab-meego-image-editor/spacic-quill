/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QSelect **************************************************************
//** This class implements Region Select query, given a data input and a **
//** well formed formula with one unbound variable.                      **   
//*************************************************************************

#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QSelect::ItemsEstimator)(int Min,int Max) = Query::MaxItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QSelect::QSelect(Query *Z,WFF *C,Region *V)
{
  Region *Buf[CoverBufSize];
  int J,N;

  XType  = QU_SELECT;
  XInput = Z;
  XCond  = C;
  XVar   = V;
  XMono  = C->Mono(V);
  XAnti  = C->Anti(V);

  // No associated are for now
  (Rect &)*this=Rect::EmptyRect();

  // Find areas covered by the query  
  *XVar=(Rect &)*XInput;
  N=XCond->Eval(XVar,Buf,CoverBufSize);

  // Compound all areas into a single rectangle
  if(N<0) (Rect &)*this=(Rect &)*XInput;
  else
    for(J=0;J<N;J++)
    {
      (Rect &)*this+=*(Rect *)Buf[J];
      delete Buf[J];
    }
}

Region *QSelect::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  int D;

  // Starting from scratch
  Q.Reset(Type(),1);
  C->TakeAll=-1;

  // Get first region from the input
  R=XInput->GetFirst(Q.In(),&D);

  // If out of input regions, return NULL
  if(!R) { Q.Reset();return(0); }

  // If the region is outside query's bounding box, return NULL
  if(!Intersects(*R)) { Q.Reset();return(0); }

  // Instantiate variable
  *XVar=*R;

  // If condition evaluates as TRUE, we are done
  if(XCond->Eval()>0)
  {
    if(XAnti) C->TakeAll=D;
    *OutDegree=D;
    return(R);
  }

  // If degree>0, the region is implicit and we return it
  if(D&&!XMono) { *OutDegree=D;return(R); }

  // Continue looking
  R=GetNext(Q,D,&D);
  if(R) *OutDegree=D; else Q.Reset();
  return(R);
}

Region *QSelect::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  int D;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  while((R=XInput->GetNext(Q.In(),InDegree,&D)))
  {
    // If our condition is antimonotonic and the next region is
    // inside previously returned one, we can return this region
    // right away, without evaluating the condition. Otherwise,
    // we must evaluate the condition.
    if(D<C->TakeAll) break; else C->TakeAll=-1;

    // If the region is outside query's bounding box, we can skip
    // over entire region's contents.
    if(!Intersects(*R)) InDegree=D;
    else
    {
      // Instantiate variable
      *XVar=*R;

      // If condition satisfied, return region
      if(XCond->Eval()>0) { if(XAnti) C->TakeAll=D;break; }

      // If our condition is monotonic and it fails on a region,
      // we can skip over entire region's contents. Otherwise, we
      // must evaluate our condition on every region of given or
      // higher degree, returning implicit regions in the process.
      if(D) { if(XMono) InDegree=D; else break; }
    }
  }

  // Done
  if(R) *OutDegree=D; else Q.Reset();
  return(R);
}

int QSelect::Items(const Region &R) const
{
  int Res;
  Res=XInput->Items((Rect)Intersect(R));
  return(Res<0? -1:(*ItemsEstimator)(0,Res));
}

int QSelect::Cost(const Region &R) const
{
  int Res;
  Res=XInput->Cost((Rect)Intersect(R));
  return(Res);
}

