/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>
using std::endl;

int QData::MeritFunc = QData::DefMeritFunc;
// Merit function used to decide which nodes to split in the Items()
// call. This is made a variable to facilitate performance experiments.

int QData::MaxLookups = QData::DefMaxLookups;
// Number of nodes to scan when computing region cover set in the Items()
// call. This is made a variable to facilitate performance experiments.

int Query::Print(std::ostream &Out)
{
  QContext Q;
  Region *R;
  int J,Count;

  Count=0;
  for(R=GetFirst(Q,&J);R;R=GetNext(Q,0,&J))
    if(!J) { Out<<*R<<endl;Count++; }
  return(Count);
}

Region *QContext::Push(Region *R,int InDegree,int *OutDegree)
{
  // Find place to add the new region
  for(;(ST!=SH)&&(DStack[Prev(ST)]<=InDegree);ST=Prev(ST));

  // Add new region to the tail
  RStack[ST]=R;
  DStack[ST]=InDegree;
  ST=Next(ST);

  // If our tail hits our head, push the head region out
  if(ST==SH)
  {
    int J=SH;
    SH=Next(J);
    *OutDegree=DStack[J];
    return(RStack[J]);
  }

  // Otherwise, we do not push out any regions
  return(0);    
}
    
Region *QContext::Pull(int InDegree,int *OutDegree)
{
  // Fall out if queue is empty or there is no explicit
  // region at the queue's tail
  if((ST==SH)||DStack[Prev(ST)]) return(0);

  // If the first region in the queue has lower degree than requested,
  // drop entire queue and fall out
  if(DStack[SH]<InDegree) { SH=ST;return(0); }

  // Pull region off the queue's head
  int J=SH;
  SH=Next(J);
  *OutDegree=DStack[J];
  return(RStack[J]);
}

//** QData ****************************************************************
//** This class abstracts spatial data structures. In addition to the    **
//** usual GetFirst()/GetNext() interface, it has Add() and Delete().    **
//*************************************************************************

QData::QData(const char *NewName)
{
  XType=QU_DATA;
  ItemCount=0;
  if(!NewName) XName=0;
  else
  {
    XName=new char[strlen(NewName)+1];
    if(XName) strcpy(XName,NewName);
  }
}

QData::~QData()
{
  if(XName) delete [] XName;
  ItemCount=0;
  XName=0;
}

int QData::Load(Query &Input)
{
  QContext Q;
  Region *R;
  int J,Count;

  Count=0;
  for(R=Input.GetFirst(Q,&J);R;R=Input.GetNext(Q,0,&J))
    if(!J) { if(Add(*R)) Count++; }
  return(Count);
}

std::ostream &operator <<(std::ostream &Out,const Query &Q)
{
  switch(Q.Type())
  {
    case QU_DATA:
    {
      QData *P=(QData *)&Q;
      Out<<(P->Name()? P->Name():"<DATA>");
    }
    break;

    case QU_SELECT:
    {
      QSelect *P=(QSelect *)&Q;
      Out<<"Select("<<*(P->Arg1())<<","
         <<*(P->Cond())<<","<<*(P->Var())<<")";
    }
    break;

    case QU_RESTRICT:
    {
      QRestrict *P=(QRestrict *)&Q;
      Out<<"Restrict("<<*(P->Arg1())<<","
         <<*(P->Cond())<<","<<*(P->Var())<<")";
    }
    break;

    case QU_NEIGHBOR:
    {
      QNeighbor *P=(QNeighbor *)&Q;
      Out<<"Neighbor("<<*(P->Arg1())<<","<<*(P->Center())<<")";
    }
    break;

    case QU_UNION:
    {
      QUnion *P=(QUnion *)&Q;
      Out<<"("<<*(P->Arg1())<<" + "<<*(P->Arg2())<<")";
    }
    break;

    case QU_INTERSCT:
    {
      QIntersect *P=(QIntersect *)&Q;
      Out<<"("<<*(P->Arg1())<<" * "<<*(P->Arg2())<<")";
    }
    break;

    case QU_DIFFRNCE:
    {
      QSubtract *P=(QSubtract *)&Q;
      Out<<"("<<*(P->Arg1())<<" - "<<*(P->Arg2())<<")";
    }
    break;

    case QU_JOIN:
    {
      QJoin *P=(QJoin *)&Q;
      Out<<"Join("<<*(P->Arg1())<<","<<*(P->Arg2())<<","<<*(P->Cond())
         <<","<<*(P->Var1())<<","<<*(P->Var2())<<")";
    }
    break;

    default: Out<<"ERROR";break;

/*
    case QU_SPLIT:
    {
      QSplit *P=(QSplit *)&Q;
      Out<<"Split("<<*(P->Arg1())<<")";
    }
    break;

    case QU_MERGE:
    {
      QMerge *P=(QMerge *)&Q;
      Out<<"Merge("<<*(P->Arg1())<<")";
    }
    break;
*/
  }

  return(Out);
}
