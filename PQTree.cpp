/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "PQTree.h"
#include <stdio.h>

PQTree::TNode::~TNode()
{
  if(NW) delete NW;
  if(NE) delete NE;
  if(SW) delete SW;
  if(SE) delete SE;
  Parent=NW=NE=SW=SE=0;
}

PQTree::PQTree(const char *NewName)
  : QData(NewName)
{
  Depth      = 0;
  ItemCount  = 0;
  Root       = 0;

  (Rect &)*this=Rect::EmptyRect();
}

PQTree::~PQTree()
{
  if(Root) delete Root;
  ItemCount=0;
  Root=0;
}

int PQTree::Add(const Region &R)
{
  double X1 = -MAXFLOAT;
  double Y1 = -MAXFLOAT;
  double X2 = MAXFLOAT;
  double Y2 = MAXFLOAT;
  double CX,CY,X,Y;
  TNode *T,*P;
  int D;

  // R must not be empty
  if(R.Empty()) return(0);

  // Must be a point
  switch(R.Type())
  {
    case REG_POINT:
      X=((Point *)&R)->X();
      Y=((Point *)&R)->Y();
      break;
    case REG_RECT:
      if(((Rect *)&R)->X1()!=((Rect *)&R)->X2()) return(0);
      if(((Rect *)&R)->Y1()!=((Rect *)&R)->Y2()) return(0);
      X=((Rect *)&R)->X1();
      Y=((Rect *)&R)->Y1();
      break;
    default:
      return(0);
  }

  // Create root if not present
  if(!Root)
  {
    Root=new TNode;
    if(!Root) return(0);
    (Rect &)*this=Rect(X,Y,X,Y);
    Root->Imp=Rect(X,Y,X,Y);
    Root->Exp=R;
    Depth=1;
    return(++ItemCount);    
  }

  // We start from the root
  P=Root;T=0;D=1;

  do
  {
    // Data in the analyzed node splits it into four quarters
    CX=P->Exp.X();
    CY=P->Exp.Y();

    // See if the data is already there
    if((X==CX)&&(Y==CY)) return(0);

    // Set parent and traverse
    T=P;
    if(X<CX)
    {
      if(Y<CY)
      {
        X2=CX;Y2=CY;D++;
        if(P->NW) P=P->NW; else { P=P->NW=new TNode;break; }
      }
      else
      {
        X2=CX;Y1=CY;D++;
        if(P->SW) P=P->SW; else { P=P->SW=new TNode;break; }
      }
    }
    else
    {
      if(Y<CY)
      {
        X1=CX;Y2=CY;D++;
        if(P->NE) P=P->NE; else { P=P->NE=new TNode;break; }
      }
      else
      {
        X1=CX;Y1=CY;D++;
        if(P->SE) P=P->SE; else { P=P->SE=new TNode;break; }
      }
    }
  }
  while(1);

  // Fall out if we failed to create a node
  if(!P) return(0);

  // Fill out the node 
  P->Parent=T;
  P->Imp=Rect(X,Y,X,Y);
  P->Exp=R;

  // Update ItemCounts and implicit regions up to the root
  for(;T;T=T->Parent)
  {
    T->Imp=T->Imp+(Rect &)R;
    T->ItemCount++;
  }

  // Correct tree depth
  if(Depth<D) Depth=D;

  // Update bounding box
  (Rect &)*this=(Rect &)*this+(Rect &)R;

  // Done
  return(++ItemCount);
}

int PQTree::Delete(const Region &R)
{
  double X1 = -MAXFLOAT;
  double Y1 = -MAXFLOAT;
  double X2 = MAXFLOAT;
  double Y2 = MAXFLOAT;
  double CX,CY,X,Y;
  TNode *P,*T;

  // Must be a point
  if(R.Type()!=REG_POINT) return(0);

  // Check if data belongs to the tree
  if(!Contains(R)) return(0);

  // Get point
  X=((Point *)&R)->X();
  Y=((Point *)&R)->Y();

  for(P=Root,T=0;P;)
  {
    // Data in the analyzed node splits it into four quarters
    CX=P->Exp.X();
    CY=P->Exp.Y();

    // See if we reached the data
    if((X==CX)&&(Y==CY)) break;

    // Set parent and traverse
    T=P;  
    if(X<CX)
    {
      if(Y<CY) { P=P->NW;X2=CX;Y2=CY; }
      else     { P=P->SW;X2=CX;Y1=CY; }
    }
    else
    {
      if(Y<CY) { P=P->NE;X1=CX;Y2=CY; }
      else     { P=P->SE;X1=CX;Y1=CY; }
    }
  }

  // Haven't found data
  if(!P) return(0);

  // @@@ WRITE NODE DELETION CODE HERE! (can't delete for now)
  return(0);
}

int PQTree::Items(const Region &R) const
{
#include "LimPriority.h"
//#include "LimBoundary.h"
//#include "LimTotal.h"
}

int PQTree::Cost(const Region &R) const
{
  // We consider cost of traversing the tree to be proportional to
  // the number of nodes to traverse.
  return(Items(R));
}

Region *PQTree::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
 
  if(!Root) { Q.Reset();return(0); }

  Q.Reset(Type());
  *OutDegree    = Depth;
  C->LastDegree = Depth;
  C->LastTNode  = Root;
  C->LastState  = 1;   

  return(&(Root->Imp));
}

Region *PQTree::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  TNode *P;

  // Need to do GetFirst() first
  if(Q.Type()!=Type()) return(0);

  // If requesting upper layer, backtrack to it
  if(C->LastDegree<=InDegree) C->LastState=6;

  do
  {
    P=C->LastTNode;
    switch(C->LastState)
    {
      case 0: // Implicit content of the node
        C->LastState++;
        *OutDegree=C->LastDegree;
        return(&(P->Imp));

      case 1: // Explicit content of the node
        C->LastState++;
        *OutDegree=0;
        return(&(P->Exp));

      case 2: // NW child
        if(!P->NW) C->LastState++;
        else
        {
          C->LastTNode=P->NW;
          C->LastDegree--;
          C->LastState=0;
        }
        break;

      case 3: // NE child
        if(!P->NE) C->LastState++;
        else
        {
          C->LastTNode=P->NE;
          C->LastDegree--;
          C->LastState=0;
        }
        break;

      case 4: // SE child
        if(!P->SE) C->LastState++;
        else
        {
          C->LastTNode=P->SE;
          C->LastDegree--;
          C->LastState=0;
        }
        break;

      case 5: // SW child
        if(!P->SW) C->LastState++;
        else
        {
          C->LastTNode=P->SW;
          C->LastDegree--;
          C->LastState=0;
        }
        break;

      case 6: // Backtrack to parent
        C->LastTNode=P->Parent;
        C->LastDegree++;
        if(C->LastTNode&&(C->LastDegree>InDegree))
          if(P==C->LastTNode->NW) C->LastState=3;
          else if(P==C->LastTNode->NE) C->LastState=4;
               else if(P==C->LastTNode->SE) C->LastState=5;
        break;
    }
  }
  while(C->LastTNode);

  // Out of items
  Q.Reset();
  return(0);
}
