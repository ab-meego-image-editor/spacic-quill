/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
// 
// LIMPRIORITY ITEMS() ALGORITHM:
// This is the totally bounded cost estimation algorithm that uses a
// priority queue, where node priority expresses how hard we want to
// decompose this node.
//
  TNode *NBuf[DefMaxLookups];
  double PBuf[DefMaxLookups];
  int CBuf[DefMaxLookups];
  int Res,I,J,L,Head,Tail;
  Region *P;
  TNode *N;

  // If R does not intersect the tree, or the tree is empty, 0 items
  if(!Root||!R.Intersects(Root->Imp)) return(0);

  // No items yet, buffer only contains root node
  Res=0;
  Head=0;
  Tail=1;
  NBuf[0] = Root;
  PBuf[0] = 0.0;
  CBuf[0] = 0;

  if(R.Intersect(Root->Imp,&P,1)>0)
  {
    CBuf[0] = (int)(Root->ItemCount*P->Area()/Root->Imp.Area());
    delete P;
  }

  for(;(Tail<=MaxLookups-4)&&(Head<Tail);Head++)
    if(R.Contains(NBuf[Head]->Imp)) Res+=NBuf[Head]->ItemCount;
    else
    {
      // See if contents of node belong to R and modify Res
      // appropriately
      if(R.Intersects(NBuf[Head]->Exp)) Res++;

      // Go through all children of a node
      for(J=0;J<4;J++)
      {
        // Get children in NW,NE,SW,SE order
        switch(J)
        {
          case 0: N=NBuf[Head]->NW;break;
          case 1: N=NBuf[Head]->NE;break;
          case 2: N=NBuf[Head]->SW;break;
          case 3: N=NBuf[Head]->SE;break;
        }

        // If this child intersects R...
        if(N&&(R.Intersect(N->Imp,&P,1)>0))
        {
          double Pri;
          int Cnt;

          double E = N->Parent->Imp.Area();
          double D = N->Imp.Area();
          double C = P->Area();

          // No longer need the intersection result
          delete P;

          // Protect against 0-area regions
          if(!C) C=D;
          if(!D) C=D=E;
          if(!E) C=D=E=1.0;

          // Compute initial estimate for N
          Cnt=(int)(N->ItemCount*C/D);

          // Compute priority
          switch(MeritFunc)
          {
            case 1:  Pri = (double)N->ItemCount/ItemCount;break;
            case 2:  Pri = (double)N->ItemCount/N->Parent->ItemCount;break;
            case 3:  Pri = (D-C)/D;break;
            case 4:  Pri = (E-C)/E;break;
            case 5:  Pri = N->ItemCount*(D-C)/D/ItemCount;break;
            case 6:  Pri = N->ItemCount*(E-C)/E/N->Parent->ItemCount;break;
            case 7:  Pri = (double)N->ItemCount/ItemCount
                         + (D-C)/D;
                     break;
            case 8:  Pri = (double)N->ItemCount/N->Parent->ItemCount
                         + (E-C)/E;

            case 0:
            default: Pri = 0.0;break;
          }

          // Find node's place in the queue
          for(I=Head+1;I<Tail;I++)
            if(Pri>PBuf[I]) break;

          // Shift data to make place for the new one
          for(L=Tail;L>I;L--)
          {
            NBuf[L]=NBuf[L-1];
            PBuf[L]=PBuf[L-1];
            CBuf[L]=CBuf[L-1];
          }

          // Add node to the queue
          NBuf[I]=N;
          PBuf[I]=Pri;
          CBuf[I]=Cnt;
          Tail++;
        }
      }
    }

  // Add item estimates for all nodes remaining in the queue
  for(;Head<Tail;Head++) Res+=CBuf[Head];

  // Done
  return(Res);
