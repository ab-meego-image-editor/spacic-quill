/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "DRTree.h"

#define ALIGN8(T) (TNode *)(((int)T+7)&-8)

DRTree::TNode::TNode()
{
  UseMask = 0x00000000;
  Leaf    = 1;
  Parent  = -1;
  IItems  = 1;
  EItems  = 0;
  Box     = Rect::EmptyRect();
}

DRTree::THead::THead()
{
  Root      = -1;
  Depth     = 0;
  ItemCount = 0;
  Box       = Rect::EmptyRect();
}

DRTree::DRTree(const char *FileName,const char *NewName)
  : QData(NewName),
    PageBuffer(FileName)
{
  THead *H;
  int J;

  // No data yet
  (Rect &)*this=Rect::EmptyRect();
  ItemCount = 0;
  Root      = -1;
  Depth     = 0;

  // Must successfully open or create a data file
  if(!OK()) return;

  // Get the header page
  if(GetFirstPage(&J,(char **)&H)>=0)
  {
    // Align pointer to 8 byte boundary
    H=(THead *)ALIGN8(H);    
    // Update metadata
    (Rect &)*this=Rect(H->Box);
    ItemCount = H->ItemCount;
    Root      = H->Root;
    Depth     = H->Depth;
    // Release header page
    ReleasePage(J);
  }
  else
  {
    // Create header page
    if(NewPage(&J,(char **)&H)<0) return;
    // Align pointer to 8 byte boundary
    H=(THead *)ALIGN8(H);    
    // Save metadata
    H->ItemCount = ItemCount;
    H->Root      = Root;
    H->Box       = (Rect &)*this;
    H->Depth     = Depth;
    // Release header page
    ReleasePage(J,1);
  }
}

DRTree::~DRTree()
{
  THead *H;
  int J;

  // Get the header page
  if(GetFirstPage(&J,(char **)&H)>=0)
  {
    // Align pointer to 8 byte boundary
    H=(THead *)ALIGN8(H);    
    // Update metadata
    H->Box       = (Rect &)*this;
    H->ItemCount = ItemCount;
    H->Root      = Root;
    H->Depth     = Depth;
    // Release header page
    ReleasePage(J,1);
  }

  ItemCount = 0;
  Root      = -1;
}

int DRTree::Add(const Region &R)
{
  TNode *T;
  int N;

  // R must not be empty
  if(R.Empty()) return(0);

  // Must be a rectangle or a point
  if((R.Type()!=REG_RECT)&&(R.Type()!=REG_POINT)) return(0);

  // For an empty tree, create root
  if(Root<0)
  {
    // Create new root node
    if(NewPage(&Root,(char **)&T)<0)
    {
      cerr<<"DRTree::Add(): Can't create root node"<<endl;
      return(0);
    }

    // Align pointer to 8 byte boundary
    T=ALIGN8(T);    

    // Initialize root page
    T->UseMask = 0x00000001;
    T->Parent  = -1;
    T->Leaf    = 1;
    T->IItems  = 1;
    T->EItems  = 1;
    T->Box     = (Rect &)R;
    T->Data[0] = (Rect &)R;

    // Mark new root page dirty and release it
    ReleasePage(Root,1);

    // Update metadata
    (Rect &)*this+=(Rect &)R;
    ItemCount=1;
    Depth=1;

    // Done
    return(ItemCount);
  }

  // Choose page to insert R into and read it from disk
  N=ChooseSubtree((Rect &)R);
  if(N<0)
  {
    cerr<<"DRTree::Add(): Can't choose a node to insert "
        <<R<<" into"<<endl;
    return(0);
  }

  // Split and insert as needed
  return(SplitAndInsert(N,(Rect &)R));
}

int DRTree::Delete(const Region &R)
{
  // @@@ ADD DELETION CODE HERE!
  return(0);
}

int DRTree::Items(const Region &R) const
{
  // This is the priority queue
  struct
  {
    int Page;   // Page number
    double Pri; // Priority
  } Buf[MaxLookups];

  TNode *T;   // Current node
  int Res;    // Result
  double Pri; // Current child's priority
  int Head;   // Nodes taken from Buf[Head]
  int Tail;   // Nodes added to Buf[Tail]
  int Mask;   // Mask of used slots in the current node
  int I,J,K,L;
  Rect RP;

  // If R does not intersect the tree, or the tree is empty, 0 items
  if((Root<0)||!Intersects(R)) return(0);

  // No result yet
  Res=0;
  Head=0;
  Tail=0;

  // Add Root to the queue
  Buf[0].Page = Root;
  Buf[0].Pri  = 0.0;
  Tail++;

  for(;Head<Tail;Head++)
    if(GetThisPage(Buf[Head].Page,(char **)&T)<0)
      cerr<<"DRTree::Items(): Can't get node "<<Buf[Head].Page<<endl;
    else
    {
      // Align node pointer
      T=ALIGN8(T);
      // If entire node is inside R, no need to traverse it
      if(R.Contains(Rect(T->Box))) Res+=T->EItems;
      else
        if(T->Leaf)
        {
          // For leaves, just count number of items that intersect R
          for(J=0,Mask=T->UseMask;Mask&&(J<RegsPerNode);J++,Mask>>=1)
            if((Mask&1)&&R.Intersects(Rect(T->Data[J]))) Res++;
        }
        else
        {
          double IntA[RegsPerNode];
          double ChiA[RegsPerNode];
          double IntT = 0.0;
          double ChiT = 0.0;
          Region *Rout;

          // Go through all children computing areas
          for(J=I=0,Mask=T->UseMask;Mask&&(J<RegsPerNode);J++,Mask>>=1)
            if(Mask&1)
            {
              // Current child
              RP=Rect(T->Data[J]);
              // Compute child's area
              ChiT+=ChiA[J]=RP.Area();
              // If child intersects R...
              if(R.Intersect(RP,&Rout,1)<=0) IntA[J]=-1.0;
              else
              {
                // One more intersecting child
                I++;
                // Compute intersection area
                IntT+=IntA[J]=Rout->Area();
                // Done with the intersection result
                delete Rout;
              }
            }

          // If intersecting children won't fit...
          if(Tail+I>MaxLookups)
          {
            // Make rough count estimate
            Res+=(int)((double)T->EItems*IntT/ChiT);
          }
          else
          {
            // Enqueue all intersecting children
            for(J=0,Mask=T->UseMask;Mask&&(J<RegsPerNode);J++,Mask>>=1)
              if((Mask&1)&&(IntA[J]>0.0))
              {
                // Compute priority
                Pri=(double)T->EItems*ChiT+ChiA[J]/ChiT;

                // Find node's place in the queue
                for(K=Head+1;K<Tail;K++)
                  if(Pri>Buf[K].Pri) break;

                // Shift data to make place for the new one
                for(L=Tail;L>K;L--) Buf[L]=Buf[L-1];

                // Add node to the queue
                Buf[K].Page = T->Child[J];
                Buf[K].Pri  = Pri;
                Tail++;
              }
          }
        }

      // We are done with the page
      ReleasePage(Buf[Head].Page);
    }

  // Done
  return(Res);
}

int DRTree::Cost(const Region &R) const
{
  // This is the priority queue
  struct
  {
    int Page;   // Page number
    double Pri; // Priority
  } Buf[MaxLookups];

  TNode *T;   // Current node
  int Res;    // Result
  double Pri; // Current child's priority
  int Head;   // Nodes taken from Buf[Head]
  int Tail;   // Nodes added to Buf[Tail]
  int Mask;   // Mask of used slots in the current node
  int I,J,K,L;
  Rect RP;

  // If R does not intersect the tree, or the tree is empty, 0 cost
  if((Root<0)||!Intersects(R)) return(0);

  // No result yet
  Res=0;
  Head=0;
  Tail=0;

  // Add Root to the queue
  Buf[0].Page = Root;
  Buf[0].Pri  = 0.0;
  Tail++;

  for(;Head<Tail;Head++)
    if(GetThisPage(Buf[Head].Page,(char **)&T)<0)
      cerr<<"DRTree::Items(): Can't get node "<<Buf[Head].Page<<endl;
    else
    {
      // Align node pointer
      T=ALIGN8(T);
      // If entire node is leaf or inside R, no need to traverse it
      if(T->Leaf||R.Contains(Rect(T->Box))) Res+=T->IItems;
      else
      {
        double IntA[RegsPerNode];
        double ChiA[RegsPerNode];
        double IntT = 0.0;
        double ChiT = 0.0;
        Region *Rout;

        // Go through all children computing areas
        for(J=I=0,Mask=T->UseMask;Mask&&(J<RegsPerNode);J++,Mask>>=1)
          if(Mask&1)
          {
            // Current child
            RP=Rect(T->Data[J]);
            // Compute child's area
            ChiT+=ChiA[J]=RP.Area();
            // If child intersects R...
            if(R.Intersect(RP,&Rout,1)<=0) IntA[J]=-1.0;
            else
            {
              // One more intersecting child
              I++;
              // Compute intersection area
              IntT+=IntA[J]=Rout->Area();
              // Done with the intersection result
              delete Rout;
            }
          }

        // If intersecting children won't fit...
        if(Tail+I>MaxLookups)
        {
          // Make rough count estimate
          Res+=(int)((double)T->IItems*IntT/ChiT);
        }
        else
        {
          // Enqueue all intersecting children
          for(J=0,Mask=T->UseMask;Mask&&(J<RegsPerNode);J++,Mask>>=1)
            if((Mask&1)&&(IntA[J]>0.0))
            {
              // Compute priority
              Pri=(double)T->IItems*ChiT+ChiA[J]/ChiT;

              // Find node's place in the queue
              for(K=Head+1;K<Tail;K++)
                if(Pri>Buf[K].Pri) break;

              // Shift data to make place for the new one
              for(L=Tail;L>K;L--) Buf[L]=Buf[L-1];

              // Add node to the queue
              Buf[K].Page = T->Child[J];
              Buf[K].Pri  = Pri;
              Tail++;
            }
        }
      }

      // We are done with the page
      ReleasePage(Buf[Head].Page);
    }

  // Done
  return(Res);
}

int DRTree::InsertToNode(int Parent,const Rect &R,int Child)
{
  TNode *T,*TC;
  int I,J,N,EC,IC;
  RectData NewBox;

  // If can't find the node, drop out
  if(GetThisPage(Parent,(char **)&T)<0)
  {
    cerr<<"DRTree::InsertToNode(): Can't find parent node "<<Parent<<endl;
    return(0);
  }

  // Align pointer to 8 byte boundary
  T=ALIGN8(T);    

  // Can't insert another node into a leaf node
  if(T->Leaf&&(Child>=0))
  {
    cerr<<"DRTree::InsertToNode(): Trying to insert child "
        <<Child<<" into leaf node "<<Parent<<endl;
    ReleasePage(Parent);
    return(0);
  }

  // Can't insert data into a non-leaf node
  if(!T->Leaf&&(Child<0))
  {
    cerr<<"DRTree::InsertToNode(): Trying to insert data "
        <<R<<" into non-leaf node "<<Parent<<endl;
    ReleasePage(Parent);
    return(0);
  }

  // If there is no free spot in the node, drop out silently
  if(T->UseMask==(1<<RegsPerNode)-1)
  {
    ReleasePage(Parent);
    return(0);
  }

  // Find free spot
  for(J=0,I=T->UseMask;(J<RegsPerNode)&&(I&1);J++,I>>=1);

  // If free spot hasn't been found, something is wrong!
  if(J==RegsPerNode)
  {
    cerr<<"DRTree::InsertToNode(): Can't find free spot in node "<<N<<endl;
    ReleasePage(Parent);
    return(0);
  }

  // Here: J = free spot

  // When inserting child node...
  if(Child>=0)
  {
    // Get child node
    if(GetThisPage(Child,(char **)&TC)>=0) TC=ALIGN8(TC);
    else
    {
      cerr<<"DRTree::InsertToNode(): Can't find child node "<<Child<<endl;
      ReleasePage(Parent);
      return(0);
    }
    // Assign child's box to the parent
    T->Data[J]=TC->Box;
    // Assign parent to the child node
    TC->Parent=Parent;
    // Compute count increments/decrements
    IC=TC->IItems;
    EC=TC->EItems;
    // Release child marking it "dirty"
    ReleasePage(Child,1);
  }
  else
  {
    // Assign data to the parent
    T->Data[J]=R;
    // Compute count increments/decrements
    IC=0;
    EC=1;
    // Update metadata
    (Rect &)*this+=R;
    ItemCount++;
  }

  // Insert data
  T->UseMask |= 1<<J;
  T->Child[J] = Child;
  T->EItems  += EC;
  T->IItems  += IC;

  // Compute new bounding box
  NewBox=T->Box+T->Data[J];

  // If bounding box has changed, update it
  if(NewBox==T->Box) I=-1; else { T->Box=NewBox;I=Parent; }

  // Release parent, marking it "dirty"
  J=T->Parent;
  ReleasePage(Parent,1);

  // Update counts and bounding boxes all the way up
  if(J>=0) UpdateCounts(J,EC,IC,I,NewBox);

  // Done
  return(ItemCount);
}

int DRTree::SplitAndInsert(int Parent,const Rect &R,int Child)
{
  TNode *T,*TC,*TN,*TT;
  int New,J,I,IC,EC;
  RectData NewBox;

  // If can't find the node, drop out
  if(GetThisPage(Parent,(char **)&T)<0)
  {
    cerr<<"DRTree::SplitAndInsert(): Can't find parent node "<<Parent<<endl;
    return(0);
  }

  // Align pointer to 8 byte boundary
  T=ALIGN8(T);

  // Can't insert another node into a leaf node
  if(T->Leaf&&(Child>=0))
  {
    cerr<<"DRTree::SplitAndInsert(): Trying to insert child "
        <<Child<<" into leaf node "<<Parent<<endl;
    ReleasePage(Parent);
    return(0);
  }

  // Can't insert data into a non-leaf node
  if(!T->Leaf&&(Child<0))
  {
    cerr<<"DRTree::SplitAndInsert(): Trying to insert data "
        <<R<<" into non-leaf node "<<Parent<<endl;
    ReleasePage(Parent);
    return(0);
  }

  // If node has free space, insert, do not split
  if(T->UseMask!=(1<<RegsPerNode)-1)
  {
    ReleasePage(Parent);
    return(InsertToNode(Parent,R,Child));
  }

  // Get child node
  if(Child>=0)
    if(GetThisPage(Child,(char **)&TC)>=0) TC=ALIGN8(TC);
    else
    {
      cerr<<"DRTree::SplitAndInsert(): Can't find child node "<<Child<<endl;
      ReleasePage(Parent);
      return(0);
    }

  // Create a new node
  if(NewPage(&New,(char **)&TN)>=0) TN=ALIGN8(TN);
  else
  {
    cerr<<"DRTree::SplitAndInsert(): Can't allocate new node"<<endl;
    if(Child>=0) ReleasePage(Child);
    ReleasePage(Parent);
    return(0);
  }

  // Set new node's fields
  TN->Leaf     = T->Leaf;
  TN->Parent   = -1;
  TN->UseMask  = (1<<(RegsPerNode/2+1))-1;
  TN->Child[0] = Child;

  // When inserting child node...
  if(Child>=0)
  {
    // Set new data and the bounding box
    TN->Data[0] = TC->Box;
    TN->Box     = TC->Box;
    // Set counts in the new node
    TN->IItems  = TC->IItems+1;
    TN->EItems  = TC->EItems;
    // Set child node's parent
    TC->Parent  = New;
    // Release the child marking it "dirty"
    ReleasePage(Child,1);
  }
  else
  {
    // Set new data and the bounding box
    TN->Data[0] = R;
    TN->Box     = R;
    // Set counts in the new node
    TN->IItems = 1;
    TN->EItems = 1;
    // Update metadata
    (Rect &)*this+=R;
    ItemCount++;
  }

  // Copy half of original node contents
  for(J=EC=IC=0;J<RegsPerNode/2;J++)
  {
    I=J+RegsPerNode-RegsPerNode/2;
    TN->Data[J+1]   = T->Data[I];
    TN->Child[J+1]  = T->Child[I];
    TN->Box        += T->Data[I];

    if(T->Leaf) EC++;
    else
    {
      // Update parent link in the child and counts in the parent
      if(GetThisPage(TN->Child[J+1],(char **)&TT)<0)
        cerr<<"DRTree::SplitAndInsert(): Can't find child node "
            <<TN->Child[J+1]<<" of parent node "<<Parent<<endl;
      else
      {
        // Align pointer to 8 byte boundary
        TT=ALIGN8(TT);
        // Compute count differential
        EC+=TT->EItems;
        IC+=TT->IItems;
        // Set new parent in the child
        TT->Parent=New;
        // Done with the child, mark "dirty" and release
        ReleasePage(TN->Child[J+1],1);
      }
    }
  }

  // Set nodes' fields
  T->UseMask  = (1<<(RegsPerNode-RegsPerNode/2))-1; 
  T->IItems  -= IC;
  T->EItems  -= EC;
  TN->IItems += IC;
  TN->EItems += EC;

  // Compute new bounding box for the old node
  NewBox=T->Data[0];
  for(J=1;J<RegsPerNode-RegsPerNode/2;J++)
    NewBox+=T->Data[J];

  // If bounding box has changed...
  if(NewBox==T->Box) I=-1; else { T->Box=NewBox;I=Parent; }

  // Release all nodes, marking them "dirty"
  J=T->Parent;
  ReleasePage(Parent,1);
  ReleasePage(New,1);

  // If old node wasn't root...
  if(J>=0)
  {
    // Update counts and bounding boxes all the way up
    UpdateCounts(J,-EC,-IC,I,NewBox);
    // Insert new node into old node's parent
    return(SplitAndInsert(J,Rect::EmptyRect(),New));
  }

  // Old node had no parent (i.e. it was root)
  // Need to create a new root

  // Nodes Child and N get inserted into new node Parent
  Child=Parent;TC=T;

  // Create new root
  if(NewPage(&Parent,(char **)&T)<0)
  {
    cerr<<"DRTree::SplitAndInsert(): Can't create new root"<<endl;
    ReleasePage(Child,1);
    ReleasePage(New,1);
    return(0);
  }

  // Align pointer to 8 byte boundary
  T=ALIGN8(T);

  // Fill out new root's fields
  T->Leaf     = 0;
  T->Parent   = -1;
  T->UseMask  = 0x00000003;
  T->Data[0]  = TC->Box;
  T->Child[0] = Child;
  T->Data[1]  = TN->Box;
  T->Child[1] = New;
  T->IItems   = TC->IItems+TN->IItems+1;
  T->EItems   = TC->EItems+TN->EItems;
  T->Box      = TC->Box+TN->Box;

  // Assign new parent to the children
  TC->Parent = Parent;
  TN->Parent = Parent;

  // Release all nodes marking them "dirty"
  ReleasePage(Parent,1);
  ReleasePage(Child,1);
  ReleasePage(New,1);

  // Update parent page number and tree depth
  Root=Parent;
  Depth++;

  // Done
  return(ItemCount);
}

int DRTree::UpdateCounts(int Node,int EItems,int IItems,int Child,RectData R)
{
  RectData NewBox;
  TNode *T;
  int J;

  // Update counts and bounding boxes all the way up
  while(Node>=0)
  {
    // Get current node
    if(GetThisPage(Node,(char **)&T)>=0) T=ALIGN8(T);
    else
    {
      cerr<<"DRTree::UpdateCounts(): Can't find node "<<Node<<endl;
      return(0);
    }

    // Update counts in the current node
    T->IItems+=IItems;
    T->EItems+=EItems;

    // If updating child bounding box...
    if(Child>=0)
    {
      // Find the child
      for(J=0;J<RegsPerNode;J++)
        if((T->UseMask&(1<<J))&&(T->Child[J]==Child)) break;
 
      // Update child's bounding box
      if(J<RegsPerNode) T->Data[J]=R;
      else
        cerr<<"DRTree::UpdateCounts(): Can't find child "<<Child
            <<" in parent "<<Node<<endl;

      // Update parent's bounding box
      NewBox=T->Box+R;
      if(NewBox==T->Box) Child=-1; else { T->Box=R=NewBox;Child=Node; }
    }
  
    // Done with the current node, shift up
    J=T->Parent;
    ReleasePage(Node,1);
    Node=J;
  }

  // Done
  return(1);
}

int DRTree::ChooseSubtree(const Rect &R)
{
  double D,MinD;
  int J,N,MinN;
  TNode *T,*TC,*MinT;
  Rect TR;

  // Get the node
  if(GetThisPage(Root,(char **)&T)<0)
  { 
    cerr<<"DRTree::ChooseSubtree(): Can't find node "<<N<<endl;
    return(-1);
  }

  // Align pointer to 8 byte boundary
  T=ALIGN8(T);

  for(N=Root;(N>=0)&&!T->Leaf;N=MinN,T=MinT)
  {
    // If node is a leaf, we found our node
    if(T->Leaf) break;

    MinD=MAXFLOAT;
    MinN=-1;
 
    // Find the most fitting rectangle to insert to
    for(J=0;J<RegsPerNode;J++)
      if(T->UseMask&(1<<J))
        if(GetThisPage(T->Child[J],(char **)&TC)<0)
          cerr<<"DRTree::ChooseSubtree(): Can't find child node "
              <<T->Child[J]<<" of parent node "<<N
              <<" (error="<<Error()<<")"<<endl;
        else
        {
          // Align pointer to 8 byte boundary
          TC=ALIGN8(TC);
          // Get rectangle corresping to the node
          TR=Rect(T->Data[J]);
          // For leaves, minimize enlargement area
          if(TC->Leaf) D=ComputeArea(TR,R);
          else
          {
            // For non-leaves, minimize the amount of overlap
            D=ComputeOver(TR,R);
            if(D==MinD) D=ComputeArea(TR,R);
          }
          // Release or keep child node depending on how good it is
          if(D>=MinD) ReleasePage(T->Child[J]);
          else
          {
            if(MinN>=0) ReleasePage(MinN);
            MinN=T->Child[J];
            MinT=TC;
            MinD=D;
          }
        }

    // Release current node
    ReleasePage(N);
  }

  // Couldn't find optimal node - something weird occured
  if(N<0)
  {
    cerr<<"DRTree::ChooseSubtree(): Can't find a leaf"<<endl;
    return(-1);
  }

  // Done
  ReleasePage(N);
  return(N);
}

double DRTree::ComputeOver(const Rect &R1,const Rect &R2) const
{
  double X,Y;

  X=R1.X1()>R2.X1()? (R2.X2()-R1.X1()):(R1.X2()-R2.X1());
  Y=R1.Y1()>R2.Y1()? (R2.Y2()-R1.Y1()):(R1.Y2()-R2.Y1());
  if((X<=0.0)||(Y<=0.0)) return(0.0);
  return(X*Y);
}

double DRTree::ComputeArea(const Rect &R1,const Rect &R2) const
{
  double X,Y;

  X=(R1.X2()>R2.X2()? R1.X2():R2.X2())-(R1.X1()<R2.X1()? R1.X1():R2.X1());
  Y=(R1.Y2()>R2.Y2()? R1.Y2():R2.Y2())-(R1.Y1()<R2.Y1()? R1.Y1():R2.Y1());
  return(X*Y-(R2.X2()-R2.X1())*(R2.Y2()-R2.Y1()));
}

Region *DRTree::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();

  // Copy initial data
  Q.Reset(Type());
  C->NextNode   = Root;
  C->NextSlot   = 0;
  C->NextDegree = Depth-1;
  memcpy(&C->LastData,&(Rect &)(*this),sizeof(Rect));

  *OutDegree=C->NextDegree+1;
  return(&C->LastData);
}

Region *DRTree::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  int LastChild,J;
  TNode *T;

  // Have to do GetFirst() first
  if(Q.Type()!=Type()) return(0);

  // When getting up, this is going to be our previous (downlink) node
  LastChild=-1;

  do
  {
    // Return 0 if out of nodes
    if(C->NextNode<0)
    {
      Q.Reset(0);
      return(0);
    }

    // Get the node
    if(GetThisPage(C->NextNode,(char **)&T)<0)
    {
      cerr<<"DRTree::GetNext(): Can't get node "<<C->NextNode<<endl;
      Q.Reset(0);
      return(0);
    }

    // Align pointer to 8 byte boundary
    T=ALIGN8(T);

    if(C->NextDegree<InDegree)
    {
      // Get up the chain, as InDegree requires
      J=T->Parent;
      ReleasePage(C->NextNode);
      LastChild=C->NextNode;
      C->NextNode=J;
      C->NextDegree++;
      T=0;
    }
    else
    {
      // If we went up, find last child node in the parent
      if(LastChild>=0)
      {
        for(J=0;J<RegsPerNode;J++)
          if((T->UseMask&(1<<J))&&(T->Child[J]==LastChild)) break;
        C->NextSlot=J+1;
      }

      // Now, find the next occupied slot
      for(J=C->NextSlot;(J<RegsPerNode)&&!(T->UseMask&(1<<J));J++);

      // If we are out of occupied slots, need to go up
      if(J<RegsPerNode) C->NextSlot=J;
      else
      {
        J=T->Parent;
        ReleasePage(C->NextNode);
        LastChild=C->NextNode;
        C->NextNode=J;
        C->NextDegree++;
        T=0;
      }
    }
  }
  while(!T);

  //
  // AT THIS POINT:
  // C->NextNode   = node where we are
  // C->NextSlot   = slot at which we look
  // C->NextDegree = degree we need to return
  // T points to the attached node
  //

  // Copy data
  C->LastData=Rect(T->Data[C->NextSlot]);

  // Store page number (need to release it later)
  J=C->NextNode;

  // Advance to the next data item (if leaf) or go deeper
  if(T->Leaf)
  {
    *OutDegree  = 0;
    C->NextSlot++;
  }
  else
  {
    *OutDegree  = C->NextDegree;
    C->NextNode = T->Child[C->NextSlot];
    C->NextSlot = 0;
    C->NextDegree--;
  }

  // Done with the page
  ReleasePage(J);

  // Return data we have just read
  return(&C->LastData);
}

void DRTree::TestTree(int Node)
{
  TNode *T,*TC;
  int IC,EC,J;

  // Start from the root
  if(Node<0) Node=Root;
  if(Node<0) return;

  if(GetThisPage(Node,(char **)&T)>=0) T=ALIGN8(T);
  else
  {
    cerr<<"DRTree::TestTree("<<Node<<"): Can't get node"<<endl;
    return;
  }

  if(T->UseMask&~((1<<RegsPerNode)-1))
    cerr<<"DRTree::TestTree("<<Node<<"): Extra bits in UseMask="
        <<(void *)T->UseMask<<endl;

  for(J=0,IC=EC=0;J<RegsPerNode;J++)
    if(T->UseMask&(1<<J))
    {
      if(!Rect(T->Box).Contains(Rect(T->Data[J])))
        cerr<<"DRTree::TestTree("<<Node<<"): "
            <<(T->Leaf? "Region":"Child")<<" "<<J
            <<" ("<<Rect(T->Data[J])<<") is out of bounds ("
            <<Rect(T->Box)<<")"<<endl;
 
      if(T->Leaf) EC++;
      else
        if(T->Child[J]<0)
          cerr<<"DRTree::TestTree("<<Node<<"): Child "<<J
              <<"points to invalid node "<<T->Child[J]<<endl;
        else  
          if(GetThisPage(T->Child[J],(char **)&TC)<0)
            cerr<<"DRTree::TestTree("<<Node<<"): Can't get child "
                <<J<<" (Node "<<T->Child[J]<<")"<<endl;
          else
          {
            TC=ALIGN8(TC);
            EC+=TC->EItems;
            IC+=TC->IItems;
          
            if(TC->Parent!=Node)
              cerr<<"DRTree::TestTree("<<Node<<"): Child "<<J
                  <<" has Parent="<<TC->Parent<<endl;

            if(TC->EItems<=0)
              cerr<<"DRTree::TestTree("<<Node<<"): Child "<<J
                  <<" has EItems="<<TC->EItems<<endl;

            if(TC->IItems<=0)
              cerr<<"DRTree::TestTree("<<Node<<"): Child "<<J
                  <<" has IItems="<<TC->IItems<<endl;

            if(T->Data[J]!=TC->Box)
              cerr<<"DRTree::TestTree("<<Node<<"): Child "<<J
                  <<" has different box"<<endl;

            ReleasePage(T->Child[J]);
            TestTree(T->Child[J]);
          }
    }

  if(EC!=T->EItems)
    cerr<<"DRTree::TestTree("<<Node<<"): Node contains "<<T->EItems
        <<" explicit regions, but its children add up to "<<EC<<endl;
  if(IC!=T->IItems-1)
    cerr<<"DRTree::TestTree("<<Node<<"): Node contains "<<T->IItems-1
        <<" implicit regions, but its children add up to "<<IC<<endl;

  ReleasePage(Node);
}
