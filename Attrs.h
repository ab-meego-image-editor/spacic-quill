/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef ATTRS_H
#define ATTRS_H

#include "Query.h"
#include "Rect.h"

#define ATTRBASESIZE 2048

class Attrs: public QData
{
  public:
    Attrs(int NewHashSize,const char *NewName=0);
    ~Attrs();

    virtual int Add(const Region &R) { return(0); }
    virtual int Delete(const Region &R) { return(0); }
    // Can't add/delete regions to attribute tables

    virtual int Items(const Region &R) const { return(ItemCount); }
    // Attrs has no spatial boundaries, so in the worst case it may
    // return everything.

    virtual int Cost(const Region &R) const;
    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

    const char *Set(const Rect &R,const char *A);
    const char *Get(const Rect &R);

  private:
    class Node
    {
      public:
        Node *Prev,*Next;
        Rect R;
        int End;
    };
    class QCType
    {
      public:
        Node *Current;
    }; 

    Node *Head;
    Node **Hash;
    int HashSize;

    int AttrSize(const char *A) const;
};

extern Attrs AttrBase;
// @@@ ATTENTION: THIS STORES ALL ATTRIBUTES FOR NOW!

#endif /* ATTRS_H */
