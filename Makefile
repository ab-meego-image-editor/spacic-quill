oS	= Region.o GNIS.o PQTree.o PRTree.o RTree.o \
	  QIntersect.o QJoin.o QNeighbor.o QRestrict.o QSelect.o \
	  QSubtract.o Query.o QUnion.o Random.o Rect.o Attrs.o \
	  WFF.o

CC=g++
CXXFLAGS=-Wall -Wextra -pedantic

all:	Test1 Test2 Test3 Test4 Test5 Test6 Test7

clean:
	rm -f *.o *.err *.tmp

Spacic:	Spacic.cpp $(oS)
	$(CC) $(CXXFLAGS) -o $@ Spacic.o $(oS)

Test1:	Test1.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test1.o $(oS)

Test2:	Test2.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test2.o $(oS)

Test3:	Test3.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test3.o $(oS)

Test4:	Test4.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test4.o $(oS)

Test5:	Test5.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test5.o $(oS)

Test6:	Test6.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test6.o $(oS)

Test7:	Test7.o $(oS)
	$(CC) $(CXXFLAGS) -o $@ Test7.o $(oS)
