/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "Random.h"
#include "Attrs.h"
#include <time.h>
#include <cstdlib>
#include <cstring>

using std::cout;
using std::endl;

Random::Random(int Points,const Rect &Bounds,const char *NewName)
  : QData(NewName)
{
  ItemCount=Points;     // Number of points to generate
  BBox=Bounds;          // Bounding box (for convinience)
  (Rect &)*this=Bounds; // Bounding box (main)
}

int Random::Items(const Region &R) const
{
  Region *Buf;
  int J;

  // Intersect bounding box with R, return 0 if no
  // intersection detected or -1 if there is an error
  J=Intersect(R,&Buf,1);
  if(J<=0) return(J);

  // Compute number of points, considering even
  // distribution across the bounding box
  J=(int)(ItemCount*Buf->Area()/Area());
cout<<"ItemCount="<<ItemCount<<" R.Area="<<Buf->Area()<<" D.Area="<<Area()<<endl;
cout<<"Random::Items() = "<<J<<endl;
  // Done
  delete Buf;
  return(J);
}

int Random::Cost(const Region &R) const
{
  return(ItemCount);
}

Region *Random::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();

  // Starting from scratch
  Q.Reset(Type());
  C->CurItem=0;

  // Initialize rand()
  srand(time(0));

  // Return next data item
  return(GetNext(Q,0,OutDegree));
}

Region *Random::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  char *P,S[256];

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  // Reset context and drop out if out of regions
  if(C->CurItem>=ItemCount) { Q.Reset();return(0); }

  // Generate new region
  CurData =
    Point
    (
      rand()*(BBox.X2()-BBox.X1())/RAND_MAX+BBox.X1(),
      rand()*(BBox.Y2()-BBox.Y1())/RAND_MAX+BBox.Y1()
    );

  // Prepare and store attributes
  P=S;
  strcpy(P,"Type");P+=strlen(P)+1;
  strcpy(P,rand()<=RAND_MAX/4? "school":"other");P+=strlen(P)+1;
  *P='\0';
  CurData.User=AttrBase.Set(CurData,S);

  C->CurItem++;
  *OutDegree=0;
  return(&CurData);
}
