/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef REGION_H
#define REGION_H

#include <iostream>

#define REG_POINT  1
#define REG_RECT   2
#define REG_POLY   3

class Region
{
  public:
    const char *User;
    // Pointer to arbitrary data that the user can set.

    Region(int NewType,const char *NewName=0);
    virtual ~Region();
    // Base constructor and destructor.

    const char *Name(void) const { return(XName); }
    // Returns name of a region or 0 for unnamed regions.
    int Type(void) const { return(XType); }
    // Returns type of a region (Point, Rectangle, Polygon, etc.).

    virtual int Empty(void) const = 0;
    // Returns 1 for empty regions.
    virtual int Intersects(const Region &R) const = 0;
    // Intersection predicate.
    virtual int Contains(const Region &R) const = 0;
    // Containment predicate.

    virtual double MinD(const Region &R) const = 0;
    // Minimal distance between two regions.
    virtual double MaxD(const Region &R) const = 0;
    // Maximal distance between two regions.
    virtual double Area(void) const = 0;
    // Area of a region.

    virtual const Region &Box(void) const = 0;
    // Bounding box of a region.
    virtual const Region &Intersect(const Region &R) const = 0;
    // Intersection of two regions.

    virtual const char *Attr(const char *Name) const = 0;
    // Fetch a named attribute of a region.

    virtual int Intersect(const Region &R,Region **Out,int Max=-1) const = 0;
    // Intersect region with R and put resulting regions (created with
    // "new") into Out. Up to Max regions are allowed in Out when
    // Max>=0. If Max<0, Out has unlimited size. This function returns
    // number of created regions on success, or -1 on failure.

    virtual int Complement(const Region &R,Region **Out,int Max=-1) const = 0;
    // Complement region with R and put resulting regions (created with
    // "new") into Out. Up to Max regions are allowed in Out when
    // Max>=0. If Max<0, Out has unlimited size. This function returns
    // number of created regions on success, or -1 on failure.

    virtual Region *Duplicate(void) const = 0;
    // Return a duplicate of a region, created with "new" operator.

    virtual const Region &operator =(const Region &R)
    { User=R.User;return(*this); }
    // Assign one region to another. This is a stub function usually
    // called after assignment between derived classes.

  protected:
    int XType;
    char *XName;
};

std::ostream &operator <<(std::ostream &Out,const Region &R);
int RemoveOverlaps(Region **Buf,int Cur,int Max=-1);

#endif /* REGION_H */
