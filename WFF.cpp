/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "WFF.h"
#include <iostream>
#include <cstring>
#include <cstdlib>

WFF::WFF(int Op,WFF *A1,WFF *A2)
{
  if((Op<OP_FALSE)||(Op>OP_NOT)) Op=OP_ILLEGAL;

  Args.L.A1 = A1;
  Args.L.A2 = A2;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,Region *A2)
{
  if((Op<OP_INTERSCT)||(Op>OP_SUBSET)) Op=OP_ILLEGAL;

  Args.S.A1 = A1;
  Args.S.A2 = A2;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,Region *A2,double C)
{
  if((Op<OP_LT)||(Op>OP_NE)) Op=OP_ILLEGAL;

  Args.D.A1 = A1;
  Args.D.A2 = A2;
  Args.D.A3 = 0;
  Args.D.A4 = 0;
  Args.D.C  = C;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,Region *A2,Region *A3,Region *A4)
{
  if((Op<OP_LT)||(Op>OP_NE)) Op=OP_ILLEGAL;

  Args.D.A1 = A1;
  Args.D.A2 = A2;
  Args.D.A3 = A3;
  Args.D.A4 = A4;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,const char *N1,double C)
{
  if((Op<OP_LT)||(Op>OP_NE)) Op=OP_ILLEGAL; else Op+=OP_ALT-OP_LT;

  Args.A.A1 = A1;
  Args.A.A2 = 0;
  Args.A.N1 = N1;
  Args.A.N2 = 0;
  Args.A.CS = 0;
  Args.A.CD = C;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,const char *N1,const char *C)
{
  if(Op!=OP_REGEXP) {
    if((Op<OP_LT)||(Op>OP_NE)) Op=OP_ILLEGAL; else Op+=OP_ALT-OP_LT;
  }

  Args.A.A1 = A1;
  Args.A.A2 = 0;
  Args.A.N1 = N1;
  Args.A.N2 = 0;
  Args.A.CS = C;
  XOp       = Op;
}

WFF::WFF(int Op,Region *A1,const char *N1,Region *A2,const char *N2)
{
  if((Op<OP_LT)||(Op>OP_NE)) Op=OP_ILLEGAL; else Op+=OP_ALT-OP_LT;

  Args.A.A1 = A1;
  Args.A.A2 = A2;
  Args.A.N1 = N1;
  Args.A.N2 = N2;
  XOp       = Op;
}

int WFF::Eval(void) const
{
  const char *S1,*S2;
  double D1,D2;
  int V1,V2;

  // For distance comparisons, compute distances
  if((XOp>=OP_LT)&&(XOp<=OP_NE))
  {
    D1=Args.D.A1->MinD(*Args.D.A2);
    D2=Args.D.A3&&Args.D.A4? Args.D.A3->MinD(*Args.D.A4):Args.D.C;
  }

  // For attribute comparisons, fetch attributes
  if((XOp>=OP_ALT)&&(XOp<=OP_ANE))
  {
    S1 = Args.A.A1->Attr(Args.A.N1);
    if(!S1) return(-1);
    S2 = Args.A.A2? Args.A.A2->Attr(Args.A.N2)
       : Args.A.CS? Args.A.CS:0;
    if(!S2&&Args.A.A2) return(-1);

    if(S2) { D1=strcmp(S1,S2);D2=0; }
    else   { D1=atof(S1);D2=Args.A.CD; }
  }

  // Evaluate based on opcode
  switch(XOp)
  {
    case OP_FALSE:    return(0);
    case OP_TRUE:     return(1);
    case OP_AND:      V1=Args.L.A1->Eval();
                      if(V1<0) return(-1);
                      V2=Args.L.A2->Eval();
                      if(V2<0) return(-1);
                      return(V1&&V2);
    case OP_OR:       V1=Args.L.A1->Eval();
                      if(V1<0) return(-1);
                      V2=Args.L.A2->Eval();
                      if(V2<0) return(-1);
                      return(V1||V2);
    case OP_NOT:      V1=Args.L.A1->Eval();
                      return(V1<0? -1:!V1);

    case OP_INTERSCT: return(Args.S.A1->Intersects(*Args.S.A2));
    case OP_SUBSETEQ: return(Args.S.A2->Contains(*Args.S.A1));
    case OP_EQUAL:    return(Args.S.A2->Contains(*Args.S.A1)&&Args.S.A1->Contains(*Args.S.A2));
    case OP_NEQUAL:   return(!Args.S.A2->Contains(*Args.S.A1)||!Args.S.A1->Contains(*Args.S.A2));
    case OP_SUBSET:   return(Args.S.A2->Contains(*Args.S.A1)&&!Args.S.A1->Contains(*Args.S.A2));

    case OP_LT: case OP_ALT: return(D1<D2);
    case OP_GT: case OP_AGT: return(D1>D2);
    case OP_LE: case OP_ALE: return(D1<=D2);
    case OP_GE: case OP_AGE: return(D1>=D2);
    case OP_EQ: case OP_AEQ: return(D1==D2);
    case OP_NE: case OP_ANE: return(D1!=D2);

    case OP_REGEXP:
      S1=Args.A.A1->Attr(Args.A.N1);
      if(!S1) return(-1);
      // @@@ ADD CODE HERE!
      return(-1);
  }

  // Wrong opcode
  return(-1);
}

int WFF::Eval(const Region *R,Region **Out,int Max) const
{
  const Region *R1;
  int I,J,K,L;
  int Orig,Comp;

  // Evaluate based on opcode
  switch(XOp)
  {
    case OP_FALSE:
      // Return nothing
      return(0);

    case OP_TRUE:
      // Return entire region
      if(!Max) return(-1);
      Out[0]=R->Duplicate();
      return(1);

    case OP_NOT:
      // Execute Eval() on the argument
      Orig=Args.L.A1->Eval(R,Out,Max);
      if(Orig<0) return(-1);
      // If no results, return original R
      if(!Orig)
      {
        if(!Max) return(-1);
        Out[0]=R->Duplicate();
        return(1);
      }
      // Complement results of Eval()
      for(I=0,Comp=0;I<Orig;I++)
      {
        J=R->Complement(*Out[I],Out+Orig+Comp,Max-Orig-Comp);
        if(J<0)
        {
          for(I=0;I<Orig+Comp;I++) delete Out[I];
          return(-1);
        }
        Comp+=J;
      }
      // Delete original results
      for(I=0;I<Orig;I++) delete Out[I];
      // Shift complemented results to the proper place
      for(I=0;I<Comp;I++) Out[I]=Out[Orig+I];
      // Done
      return(Comp);
      
    case OP_AND:
      // Execute Eval() on the first argument
      Orig=Args.L.A1->Eval(R,Out,Max);
      if(Orig<0) return(-1);
      if(!Orig)  return(0);
      // Execute Eval() on the second argument
      Comp=Args.L.A2->Eval(R,Out+Orig,Max-Orig);
      // Second Eval() failed, clean up and exit
      if(Comp<=0)
      {
        for(I=0;I<Orig;I++) delete Out[I];
        return(Comp);
      }
      // Second Eval() successful, compute intersections
      for(I=K=0;I<Orig;I++)
        for(J=0;J<Comp;J++)
        {
          L=Out[I]->Intersect(*Out[Orig+J],Out+Orig+Comp+K,Max-Orig-Comp-K);
          if(L>=0) K+=L;
          else
          {
            // Error occured, clean up and exit
            for(I=0;I<Orig+Comp+K;I++) delete Out[I];
            return(-1);
          }
        }
      // Delete results of initial Eval()s
      for(I=0;I<Orig+Comp;I++) delete Out[I];
      // Shift intersection results back
      for(I=0;I<K;I++) Out[I]=Out[Orig+Comp+I];
      // Done
      return(K);
      
    case OP_OR:
      // Execute Eval() on the first argument
      J=Args.L.A1->Eval(R,Out,Max);
      if(J<0) return(-1);
      // Execute Eval() on the second argument
      I=Args.L.A2->Eval(R,Out+J,Max-J);
      // Second Eval() successful, return union of results
      if(I>=0) return(I+J);
      // Second Eval() failed, clean up
      for(I=0;I<J;I++) delete Out[I];
      return(-1);

    case OP_INTERSCT:
    case OP_SUBSETEQ:
      if((R!=Args.S.A1)&&(R!=Args.S.A2)) return(-1);
      return(Args.S.A1->Intersect(*Args.S.A2,Out,Max));

    case OP_EQUAL:
      if((R!=Args.S.A1)&&(R!=Args.S.A2)) return(-1);
      R1=(R==Args.S.A1)? Args.S.A2:Args.S.A1;
      // If R does not contain other region, return nothing
      if(!R->Contains(*R1)) return(0);
      // If R contains other region, return other region
      if(!Max) return(-1);
      Out[0]=R1->Duplicate();
      return(1);

    case OP_NEQUAL:
      if((R!=Args.S.A1)&&(R!=Args.S.A2)) return(-1);
      R1=(R==Args.S.A1)? Args.S.A2:Args.S.A1;
      // If R contains other region, return complement
      if(!R->Contains(*R1)) return(R->Complement(*R1,Out,Max));
      // If R does not contain other region, return whole R
      if(!Max) return(-1);
      Out[0]=R->Duplicate();
      return(1);
  }

  // Remaining operators are not supported
  return(-1);  
}

int WFF::Mono(const Region *R) const
{
  // No R or R.op.R in S-expression
  if((XOp>=OP_INTERSCT)&&(XOp<=OP_SUBSET))
  {
    if((Args.S.A1!=R)&&(Args.S.A2!=R)) return(1);
    if((Args.S.A1==R)&&(Args.S.A2==R)) return(1);
  }

  // No R or d(R,R) in D-expression
  if((XOp>=OP_LT)&&(XOp<=OP_NE))
  {
    if((Args.D.A1!=R)&&(Args.D.A2!=R)&&(Args.D.A3!=R)&&(Args.D.A4!=R))
      return(1);
    if((Args.D.A1==R)&&(Args.D.A2==R)) return(1);
    if((Args.D.A3==R)&&(Args.D.A4==R)) return(1);
  }

  // No R in A-expression
  if((XOp>=OP_ALT)&&(XOp<=OP_REGEXP))
    if((Args.A.A1!=R)&&(Args.A.A2!=R)) return(1);

  switch(XOp)
  {
    case OP_TRUE:
    case OP_FALSE:    return(1);
    case OP_OR:
    case OP_AND:      return(Args.L.A1->Mono(R)&&Args.L.A2->Mono(R));
    case OP_NOT:      return(Args.L.A1->Anti(R));
    case OP_INTERSCT: return(1);
    case OP_SUBSETEQ:
    case OP_SUBSET:   return(Args.S.A2==R);

    case OP_LT:
    case OP_LE:
      if((R==Args.D.A1)||(R==Args.D.A2))
        if((R!=Args.D.A3)&&(R!=Args.D.A4)) return(1);
      return(0);
    case OP_GT:
    case OP_GE:
      if((R==Args.D.A3)||(R==Args.D.A4))
        if((R!=Args.D.A1)&&(R!=Args.D.A2)) return(1);
      return(0);
  }

  // Remaining operators are not monotonic
  return(0);
}

int WFF::Anti(const Region *R) const
{
  // No R or R.op.R in S-expression
  if((XOp>=OP_INTERSCT)&&(XOp<=OP_SUBSET))
  {
    if((Args.S.A1!=R)&&(Args.S.A2!=R)) return(1);
    if((Args.S.A1==R)&&(Args.S.A2==R)) return(1);
  }

  // No R or d(R,R) in D-expression
  if((XOp>=OP_LT)&&(XOp<=OP_NE))
  {
    if((Args.D.A1!=R)&&(Args.D.A2!=R)&&(Args.D.A3!=R)&&(Args.D.A4!=R))
      return(1);
    if((Args.D.A1==R)&&(Args.D.A2==R)) return(1);
    if((Args.D.A3==R)&&(Args.D.A4==R)) return(1);
  }

  // No R in A-expression
  if((XOp>=OP_ALT)&&(XOp<=OP_REGEXP))
    if((Args.A.A1!=R)&&(Args.A.A2!=R)) return(1);

  switch(XOp)
  {
    case OP_TRUE:
    case OP_FALSE:    return(1);
    case OP_AND:
    case OP_OR:       return(Args.L.A1->Anti(R)&&Args.L.A2->Anti(R));
    case OP_NOT:      return(Args.L.A1->Mono(R));
    case OP_INTERSCT: return(0);
    case OP_SUBSETEQ:
    case OP_SUBSET:   return(Args.S.A1==R);

    case OP_LT:
    case OP_LE:
      if((R==Args.D.A3)||(R==Args.D.A4))
        if((R!=Args.D.A1)&&(R!=Args.D.A2)) return(1);
      return(0);
    case OP_GT:
    case OP_GE:
      if((R==Args.D.A1)||(R==Args.D.A2))
        if((R!=Args.D.A3)&&(R!=Args.D.A4)) return(1);
      return(0);
  }

  // Remaining operators are not anti-monotonic
  return(0);
}

void WFF::operator delete(void *P)
{
  WFF *F=(WFF *)P;

  // Delete children
  switch(F->XOp)
  {
    case OP_AND:
    case OP_OR:
    case OP_NOT:
      if(F->Args.L.A1) delete F->Args.L.A1;
      if(F->Args.L.A2) delete F->Args.L.A2;
      break;
  }

  // Delete itself
  ::delete(F);
}

WFF *WFF::Normalize(WFF *F)
{
  WFF *L1,*L2,*S1,*F1;

  switch(F->XOp)
  {
    case OP_AND:
      F->Args.L.A1=Normalize(F->Args.L.A1);
      F->Args.L.A2=Normalize(F->Args.L.A2);
      // FALSE AND F
      if(F->Args.L.A1->XOp==OP_FALSE)
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;  
        return(F1);
      }
      // F AND FALSE
      if(F->Args.L.A2->XOp==OP_FALSE)
      {
        F1=F->Args.L.A2;
        F->Args.L.A2=0;
        delete F;   
        return(F1);
      }
      // TRUE AND F
      if(F->Args.L.A1->XOp==OP_TRUE)
      {
        F1=F->Args.L.A2;
        F->Args.L.A2=0;
        delete F;
        return(F1);
      }
      // F AND TRUE
      if(F->Args.L.A2->XOp==OP_TRUE) 
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;  
        return(F1);
      }
      // F AND F
      if(F->Args.L.A1==F->Args.L.A2)
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=F->Args.L.A2=0;
        delete F;
        return(F1);
      }
      // F AND NOT F
      if((F->Args.L.A2->XOp==OP_NOT)&&(F->Args.L.A1==F->Args.L.A2->Args.L.A1))
      {
        delete F->Args.L.A2;
        F->Args.L.A1=0;
        F->Args.L.A2=0;
        F->XOp=OP_FALSE;
        return(F);
      }
      // NOT F AND F
      if((F->Args.L.A1->XOp==OP_NOT)&&(F->Args.L.A2==F->Args.L.A1->Args.L.A1))
      {
        delete F->Args.L.A1;
        F->Args.L.A1=0;
        F->Args.L.A2=0;
        F->XOp=OP_FALSE;
        return(F);
      }
      break;

    case OP_OR:
      F->Args.L.A1=Normalize(F->Args.L.A1);
      F->Args.L.A2=Normalize(F->Args.L.A2);
      // TRUE OR F
      if(F->Args.L.A1->XOp==OP_TRUE)
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;   
        return(F1);
      }
      // F OR TRUE
      if(F->Args.L.A2->XOp==OP_TRUE)
      {
        F1=F->Args.L.A2;
        F->Args.L.A2=0;
        delete F;
        return(F1);
      }
      // FALSE OR F
      if(F->Args.L.A1->XOp==OP_FALSE) 
      {
        F1=F->Args.L.A2;
        F->Args.L.A2=0;
        delete F;  
        return(F1);
      }
      // F OR FALSE
      if(F->Args.L.A2->XOp==OP_FALSE)
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;
        return(F1);
      }
      // F OR F
      if(F->Args.L.A1==F->Args.L.A2)
      {
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;
        return(F1);
      }
      // F OR NOT F
      if((F->Args.L.A2->XOp==OP_NOT)&&(F->Args.L.A1==F->Args.L.A2->Args.L.A1))
      {
        delete F->Args.L.A2;
        F->Args.L.A1=0;
        F->Args.L.A2=0;
        F->XOp=OP_TRUE;
        return(F);
      }
      // NOT F OR F
      if((F->Args.L.A1->XOp==OP_NOT)&&(F->Args.L.A2==F->Args.L.A1->Args.L.A1))
      {
        delete F->Args.L.A1;
        F->Args.L.A1=0;
        F->Args.L.A2=0;
        F->XOp=OP_TRUE;
        return(F);
      }
      break;

    case OP_NOT:
      F->Args.L.A1=Normalize(F->Args.L.A1);
      // NOT FALSE 
      if(F->Args.L.A1->XOp==OP_FALSE)
      {
        F->Args.L.A1->XOp=OP_TRUE;
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;
        return(F1);
      }
      // NOT TRUE
      if(F->Args.L.A1->XOp==OP_TRUE)
      {
        F->Args.L.A1->XOp=OP_FALSE;
        F1=F->Args.L.A1;
        F->Args.L.A1=0;
        delete F;
        return(F1);
      }
      // NOT NOT F
      if(F->Args.L.A1->XOp==OP_NOT)
      {
        F1=F->Args.L.A1->Args.L.A1;
        F->Args.L.A1->Args.L.A1=0;
        delete F;
        return(F1);
      }
      break;

    case OP_SUBSET:
      S1=new WFF(OP_SUBSETEQ,F->Args.S.A2,F->Args.S.A1);
      L1=new WFF(OP_NOT,S1,0);
      L2=new WFF(OP_AND,F,L1);
      F->XOp=OP_SUBSETEQ;
      return(L2);

    case OP_EQUAL:
      S1=new WFF(OP_SUBSETEQ,F->Args.S.A2,F->Args.S.A1);
      L1=new WFF(OP_AND,F,S1);
      F->XOp=OP_SUBSETEQ;
      return(L1);

    case OP_NEQUAL:
      S1=new WFF(OP_SUBSETEQ,F->Args.S.A2,F->Args.S.A1);
      L1=new WFF(OP_AND,F,S1);
      L2=new WFF(OP_NOT,L1,0);
      F->XOp=OP_SUBSETEQ;
      return(L2);

    case OP_EQ:
      F1=new WFF(*F);
      L1=new WFF(OP_OR,F,F1);
      L2=new WFF(OP_NOT,L1,0);
      F->XOp=OP_GT;
      F1->XOp=OP_LT;
      return(L2);

    case OP_NE:
      F1=new WFF(*F);
      L1=new WFF(OP_OR,F,F1);
      F->XOp=OP_GT;
      F1->XOp=OP_LT;
      return(L1);

    case OP_LE:
      L1=new WFF(OP_NOT,F,0);
      F->XOp=OP_GT;
      return(L1);

    case OP_GE:
      L1=new WFF(OP_NOT,F,0);
      F->XOp=OP_LT;
      return(L1);
  }

  // Done!
  return(F);
}

std::ostream &operator <<(std::ostream &Out,const WFF &F)
{
  static const char *DOps[] = { "<",">","<=",">=","=","<>" };
  static const char *SOps[] = { "INTERSCT","SUBSETEQ","=","<>","SUBSET" };

  switch(F.XOp)
  {
    case OP_TRUE:  Out<<"TRUE";break;
    case OP_FALSE: Out<<"FALSE";break;
    case OP_AND:   Out<<"("<<*(F.Args.L.A1)<<") AND ("<<*(F.Args.L.A2)<<")";break;
    case OP_OR:    Out<<"("<<*(F.Args.L.A1)<<") OR ("<<*(F.Args.L.A2)<<")";break;
    case OP_NOT:   Out<<"NOT ("<<*(F.Args.L.A1)<<")";break;

    case OP_INTERSCT:
    case OP_SUBSETEQ:
    case OP_SUBSET:
    case OP_EQUAL:
    case OP_NEQUAL:
      Out<<*(F.Args.S.A1)<<" "<<SOps[F.XOp-OP_INTERSCT]<<" "<<*(F.Args.S.A2);
      break;

    case OP_LT:
    case OP_GT:
    case OP_LE:
    case OP_GE:
    case OP_EQ:
    case OP_NE:
      Out<<"d("<<*(F.Args.D.A1)<<","<<*(F.Args.D.A2)
         <<") "<<DOps[F.XOp-OP_LT]<<" ";
      if(F.Args.D.A3&&F.Args.D.A4)
        Out<<"d("<<*(F.Args.D.A3)<<","<<*(F.Args.D.A4)<<")";
      else
        Out<<F.Args.D.C;
      break;

    case OP_ALT:
    case OP_AGT:
    case OP_ALE:
    case OP_AGE:
    case OP_AEQ:
    case OP_ANE:
      Out<<*(F.Args.A.A1)<<"."<<F.Args.A.N1<<DOps[F.XOp-OP_ALT];
      if(F.Args.A.A2) Out<<*(F.Args.A.A2)<<"."<<F.Args.A.N2;
      else if(F.Args.A.CS) Out<<"\""<<F.Args.A.CS<<"\"";
           else Out<<F.Args.A.CD;          
      break;

    default: Out<<"ERROR";break;
  }

  return(Out);
}
