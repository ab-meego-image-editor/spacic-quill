/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef QUERY_H
#define QUERY_H

#include "Rect.h"
#include "WFF.h"
#include <iostream>

#define QU_DATA       1
#define QU_SELECT     2
#define QU_RESTRICT   3
#define QU_NEIGHBOR   4
#define QU_SPLIT      5
#define QU_MERGE      6
#define QU_UNION      7
#define QU_INTERSCT   8
#define QU_DIFFRNCE   9
#define QU_JOIN       10

//** QContext *************************************************************
//** This is a query context, or a "cursor" that stores query execution  **
//** state. It also contains mechanism for buffering that reduces amount **
//** of GetNext() calls.                                                 ** 
//*************************************************************************
class QContext
{
  public:
    enum { BufSize=240 };
    enum { StackSize=16 };
    enum { MaxInputs=2 };

    QContext()
    {
      int J;
      for(J=0;J<MaxInputs;J++) XInput[J]=0;
      XImpItems=XExpItems=0;
      SH=ST=0;XType=0;
    }

    ~QContext()
    {
      int J;
      for(J=0;J<MaxInputs;J++)
        if(XInput[J]) { delete XInput[J];XInput[J]=0; }
      XImpItems=XExpItems=0;
      SH=ST=0;XType=0;
    }

    void Reset(int NewType=0,int Args=0)
    {
      int J;
      if(Args>MaxInputs) Args=MaxInputs;
      for(J=0;J<Args;J++)
        if(!XInput[J]) XInput[J]=new QContext;
      XImpItems=XExpItems=0;
      XType=NewType;SH=ST=0;
    }

    int Type(void) const        { return(XType); }
    void *Data(void) const      { return((void *)XData); }
    QContext &In(int N=0) const { return(*XInput[N]); }
    int ImpItems(void) const    { return(XImpItems); }
    int ExpItems(void) const    { return(XExpItems); }

    Region *Push(Region *R,int InDegree,int *OutDegree);
    // Push a region on stack. Returns excessive region if overflow
    // happens, returns 0 otherwise.

    Region *Pull(int InDegree,int *OutDegree);
    // Pull a region off stack. Returns 0 if stack is empty,
    // returns a region otherwise.

  private:
    int XType;           // Type of the running query
    int ST,SH;           // Stack head and tail
    int XImpItems;       // Number of implicit items returned so far
    int XExpItems;       // Number of explicit items returned so far

    QContext *XInput[MaxInputs];  // Contexts of the input queries

    Region *RStack[StackSize];    // Region stack
    int DStack[StackSize];        // Degree stack

    // Query-specific data buffer, double-aligned
    double XData[BufSize/sizeof(double)];

    int Next(int N) { return((N+1)%StackSize); }
    int Prev(int N) { return((N? N:StackSize)-1); }
};

//** Query ****************************************************************
//** This class defines generic query interface. All other data source   **
//** and query classes are derived from this class.                      **
//*************************************************************************
class Query: public Rect
{
  public:
    enum { CoverBufSize=256 };
    // Size of a region buffer used in some Items() calls.

    int Type(void) const { return(XType); }
    // Return query type.

    virtual int Cost(const Region &R) const = 0;
    // Return estimated cost of executing the query if we are only
    // interested in results inside a given region R. Returns -1
    // in the case of error.

    virtual int Items(const Region &R) const = 0;
    // Return estimated number of explicit regions returned
    // from the query in a given region R. Returns -1 in the
    // case of error.

    virtual Region *GetFirst(QContext &Q,int *OutDegree) = 0;
    // Get first region from the query result and set OutDegree to
    // the degree of this region. Return NULL if the query result
    // is empty.

    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree) = 0;
    // Get next region from the query result with a degree no less
    // than the one passed via InDegree. Set OutDegree to the degree
    // of this region. Return NULL if there are no more satisfying
    // regions in the query result.

    // NOTE:
    // _Degree_ of a region is an integer such that for every region
    // A containing region B, degree(A)>degree(B). All explicit regions
    // have degree 0. GetFirst/GetNext always return contents of regions
    // with higher than given degree (i.e. 2 0 1 1 1 1 2 0 1 1 1 1 2...
    // for InDegree=1 in a quadtree, for example).

    int Print(std::ostream &Out);
    // Print out result of query execution. Returns number of explicit
    // regions in the result.

    int Items(void) const { return(Items((Rect &)*this)); }
    // Return estimated total number of explicit regions returned
    // from the query. This is just a convenient wrapper around
    // Items(Region).

    int Cost(void) const { return(Cost((Rect &)*this)); }
    // Return estimated cost of executing the query. This is just
    // a convenient wrapper around Cost(Region).

    static int MinItems(int Min,int Max)
    { return((Min<0)||(Max<0)? -1:Min); }
    static int MaxItems(int Min,int Max)
    { return((Min<0)||(Max<0)? -1:Max); }
    static int AvgItems(int Min,int Max)
    { return((Min<0)||(Max<0)? -1:(Min+Max)/2); }
    // Different estimator functions for Items(). Use one
    // of these functions or supply your own.

  protected:
    int XType;
};

//** QData ****************************************************************
//** This class abstracts spatial data structures. In addition to the    **
//** usual GetFirst()/GetNext() interface, it has Add() and Delete().    **
//*************************************************************************
class QData: public Query
{
  public:
    enum { DefMaxLookups=60 };
    // Default number of nodes to scan when computing region cover
    // set in the Items() call.
    enum { DefMeritFunc=0 };
    // Default merit function used to decide which nodes to split
    // in the Items() call.

    static int MaxLookups;
    // Number of nodes to scan when computing region cover set
    // in the Items() call. This is made a variable to facilitate
    // performance experiments.

    static int MeritFunc;
    // Merit function used to decide which nodes to split in the
    // Items() call. This is made a variable to facilitate
    // performance experiments.

    QData(const char *NewName);
    ~QData();

    const char *Name(void) { return(XName); }
    // Data structures can be named, so that one can later select
    // an appropriate data source by its name.

    virtual int Add(const Region &R) = 0;
    virtual int Delete(const Region &R) = 0;
    // Unlike with queries, one can add and delete items from a data
    // structure. Add() returns the number of items in the data
    // structure on success, or 0 on failure. Delete() returns 1 on
    // success, or 0 on failure.

    int Load(Query &Input);
    // Load result of query execution into the data structure. Returns
    // number of explicit regions added.

  private:
    char *XName;

  protected:
    int ItemCount;   // ATTENTION: Add()/Delete() must modify this!
};

//** QSelect **************************************************************
//** This class implements Region Select query, given a data input and a **
//** well formed formula with one unbound variable.                      **   
//*************************************************************************
class QSelect: public Query
{
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QSelect(Query *Z,WFF *C,Region *V);

    Query  *Arg1(void) const { return(XInput); }
    WFF    *Cond(void) const { return(XCond); }
    Region *Var(void) const  { return(XVar); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType { public: int TakeAll; };

    Query  *XInput;
    WFF    *XCond;
    Region *XVar;
    int XMono,XAnti;
};

//** QRestrict ************************************************************
//** This class implements Region Restriction query, given a data input  **
//** and a well formed formula with one unbound variable.                **   
//*************************************************************************
class QRestrict: public Query
{
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QRestrict(Query *Z,WFF *C,Region *V);

    Query  *Arg1(void) const { return(XInput); }
    WFF    *Cond(void) const { return(XCond); }
    Region *Var(void) const  { return(XVar); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    enum { BufSize=(QContext::BufSize-2*sizeof(int))/sizeof(Region *) };
    class QCType
    {
      public:
        // This must be BufSize, but CBuilder is anal about it.
        Region *Buffer[(QContext::BufSize-2*sizeof(int))/sizeof(Region *)];
        int Items,Current;
    };

    Query *XInput;
    WFF *XCond;
    Region *XVar;
    int XMono,XAnti;
};

//** QNeighbor ************************************************************
//** This class implements Nearest Neighbor query, given a key region.   **
//*************************************************************************
class QNeighbor: public Query
{
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QNeighbor(Query *NewInput,Region *NewCenter);

    Query  *Arg1(void) const   { return(XInput); }
    Region *Center(void) const { return(XCenter); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType { public: double MinD; };

    Query *XInput;
    Region *XCenter;
};

//** QUnion ***************************************************************
//** This class implements Union query, given two data inputs.           **
//*************************************************************************
class QUnion: public Query
{ 
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.
 
    QUnion(Query *NewInput1,Query *NewInput2);

    Query *Arg1(void) const { return(XInput1); }
    Query *Arg2(void) const { return(XInput2); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType
    {
      public:
        Query *CurInput,*AltInput;
        int CurAdjust,AltAdjust;
    };

    Query *XInput1,*XInput2;
};

//** QIntersect ***********************************************************
//** This class implements Intersect query, given two data inputs.       **
//*************************************************************************
class QIntersect: public Query
{ 
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QIntersect(Query *NewInput1,Query *NewInput2);

    Query *Arg1(void) const { return(XInput1); }
    Query *Arg2(void) const { return(XInput2); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType { public: Query *CurInput,*AltInput; };

    Query *XInput1,*XInput2;
};

//** QSubtract ************************************************************
//** This class implements Subtract query, given two data inputs.        **
//*************************************************************************
class QSubtract: public Query
{ 
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QSubtract(Query *NewInput1,Query *NewInput2);

    Query *Arg1(void) const { return(XInput1); }
    Query *Arg2(void) const { return(XInput2); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType { public: Query *CurInput,*AltInput; };

    Query *XInput1,*XInput2;
};

//** QJoin ****************************************************************
//** This class implements Join query, given two data inputs and a       **
//** well formed formula with two free variables.                        **
//*************************************************************************
class QJoin: public Query
{ 
  public:
    static int (*ItemsEstimator)(int Min,int Max);
    // By setting this pointer to MinItems, MaxItems, AvgItems,
    // or some other estimator function, the user can control
    // the return value of the Items() call.

    QJoin(Query *Z1,Query *Z2,WFF *C,Region *R1,Region *R2);

    Query  *Arg1(void) const { return(XInput1); }
    Query  *Arg2(void) const { return(XInput2); }
    WFF    *Cond(void) const { return(XCond); }
    Region *Var1(void) const { return(XVar1); }
    Region *Var2(void) const { return(XVar2); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    class QCType
    {
      public:
        Query *Z1,*Z2;
        Region *R1,*R2;
        Rect Box;
    };

    Query  *XInput1,*XInput2;
    Region *XVar1,*XVar2;
    WFF    *XCond;
    int    XMono,XAnti;
};

class QSplit: public Query
{
  public:
    QSplit(Query *NewInput);

    Query *Arg1(void) const { return(XInput); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    Query *XInput;
};

class QMerge: public Query
{
  public:
    QMerge(Query *NewInput);

    Query *Arg1(void) const { return(XInput); }

    virtual int Items(const Region &R) const;
    virtual int Cost(const Region &R) const;

    virtual Region *GetFirst(QContext &Q,int *OutDegree);
    virtual Region *GetNext(QContext &Q,int InDegree,int *OutDegree);

  private:
    Query *XInput;
};

std::ostream &operator <<(std::ostream &Out,const Query &Q);

#endif /* QUERY_H */
