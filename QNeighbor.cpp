/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
//** QNeighbor ************************************************************
//** This class implements Nearest Neighbor query, given a key region.   **
//*************************************************************************

#include "Query.h"
#include "Rect.h"
#include <iostream>
#include <string.h>

int (*QNeighbor::ItemsEstimator)(int Min,int Max) = Query::MinItems;
// By setting this pointer to MinItems, MaxItems, AvgItems, or some
// other estimator function, the user can control the return value
// of the Items() call.

QNeighbor::QNeighbor(Query *NewInput,Region *NewCenter)
{
  XType   = QU_NEIGHBOR;
  XInput  = NewInput;
  XCenter = NewCenter;

  (Rect &)*this=(Rect &)*NewInput;
}

Region *QNeighbor::GetFirst(QContext &Q,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  double Dist;
  int J,D;

  // Starting from scratch
  Q.Reset(Type(),1);
  C->MinD=MAXFLOAT;

  // Find minimal distance
  R=XInput->GetFirst(Q.In(),&D);
  for(;R;R=XInput->GetNext(Q.In(),J,&D))
    if(D)
    {
      // Handle implicit region
      J=D;
      if(!XCenter->Contains(*R))
      {
        Dist=XCenter->MinD(*R);
        if(Dist<C->MinD) J=0;
      }
    }
    else
    {
      // Handle explicit region
      if(!XCenter->Intersects(*R))
      {
        Dist=XCenter->MinD(*R);
        if(Dist<C->MinD) C->MinD=Dist;
      }
    }

  // Get first region
  R=XInput->GetFirst(Q.In(),&D);

  // Out of regions, scan finished
  if(!R) { Q.Reset();return(0); }

  // See if we need to return the first region
  if(!D)
  {
    if(!XCenter->Intersects(*R)&&(XCenter->MinD(*R)<=C->MinD))
    { *OutDegree=D;return(R); }
  }
  else
  {
    if(!XCenter->Contains(*R)&&(XCenter->MinD(*R)<=C->MinD))
    { *OutDegree=D;return(R); }
  }

  // Search further
  R=GetNext(Q,D,&D);
  if(R) *OutDegree=D; else Q.Reset();
  return(R);
}

Region *QNeighbor::GetNext(QContext &Q,int InDegree,int *OutDegree)
{
  QCType *C=(QCType *)Q.Data();
  Region *R;
  int D;

  // Out of regions, need to call GetFirst() to restart
  if(Q.Type()!=Type()) return(0);

  // Get next region
  while((R=XInput->GetNext(Q.In(),InDegree,&D)))
  {
    if(!D)
    {
      if(!XCenter->Intersects(*R)&&(XCenter->MinD(*R)<=C->MinD)) break;
    }
    else
    {
      if(!XCenter->Contains(*R)&&(XCenter->MinD(*R)<=C->MinD)) break;
      else InDegree=D;
    }
  }

  // Done
  if(R) *OutDegree=D; else Q.Reset();
  return(R);
}

int QNeighbor::Items(const Region &R) const
{
  int Res;

  // Nearest Neighbor query usually returns a single region, if any
  Res=XInput->Items(R);

  return(Res<0? -1:(*ItemsEstimator)(Res? 1:0,Res));
}

int QNeighbor::Cost(const Region &R) const
{
  // @@@ ADD STUFF HERE!
  return(-1);
}
