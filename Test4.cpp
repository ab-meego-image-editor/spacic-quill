/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "PRTree.h"
#include "PQTree.h"
#include "Query.h"
#include <iostream>

using std::cout;
using std::endl;

//
// Testing QUnion class.
//

int main(int argc,const char *argv[])
{
  QContext QC;
  PRTree T1(Rect(-1024.0,-1024.0,1024.0,1024.0));
  PQTree T2;
  QUnion Q1(&T1,&T2);
  QUnion Q2(&T2,&T1);
  Region *R;
  Point P;
  int J;

  for(J=1;J<=20;J++)
  {
    P=Point(-J,-J); T1.Add(P);
    P=Point(-J,J);  T1.Add(P);
    P=Point(J,-J);  T1.Add(P);
    P=Point(J,J);   T1.Add(P);
  }
  for(J=21;J<=40;J++)
  {
    P=Point(-J,-J); T2.Add(P);
    P=Point(-J,J);  T2.Add(P);
    P=Point(J,-J);  T2.Add(P);
    P=Point(J,J);   T2.Add(P);
  }

  cout<<"QUnion 1:"<<endl;
  for(R=Q1.GetFirst(QC,&J);R;R=Q1.GetNext(QC,0,&J))
    if(!J) cout<<*R<<endl;
//    cout<<J<<":\t"<<*R<<endl;

  cout<<"QUnion 2:"<<endl;
  for(R=Q2.GetFirst(QC,&J);R;R=Q2.GetNext(QC,0,&J))
    if(!J) cout<<*R<<endl;
//    cout<<J<<":\t"<<*R<<endl;

  return(0);
}

