/*
 * Spacic, spatial database
 * ------------------------
 *
 * Copyright (c) 2000, Marat Fayzullin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *     * Neither the name of the Marat Fayzullin nor the names of project's
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "PRTree.h"
#include "PQTree.h"
#include "RTree.h"
#include "Query.h"
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

//
// Performance tests.
//

#define PI 3.14159265

double Digits(char *S,int N)
{
  int J;
  for(J=0;N;N--,S++) J=J*10+(*S-'0');
  return((double)J);
}

int main(int argc,const char *argv[])
{
  QContext QC;
  PRTree T1(Rect(-1024.0,-1024.0,1024.0,1024.0));
  PQTree T2;
  RTree  T3;

  Rect Box1,Box2,Box3,Box4,Box5,Box6,Var;

  WFF A1(OP_SUBSETEQ,&Var,&Box1);
  WFF A2(OP_INTERSCT,&Var,&Box2);
  WFF A3(OP_LE,&Var,&Box3,&Var,&Box4);
  WFF A4(OP_GE,&Var,&Box5,&Var,&Box6);
  WFF A5(OP_AND,&A3,&A4);
  WFF A6(OP_AND,&A5,&A2);
  WFF A7(OP_AND,&A6,&A1);

  QSelect Q1(&T1,&A7,&Var);
  QSelect Q2(&T2,&A7,&Var);
  QSelect Q3(&T3,&A7,&Var);
  QSubtract Q4(&T3,&T2);
  QNeighbor Q5(&Q4,&Box1);

  Region *R;
  Point P;
  FILE *F;

  char S[1024];
  double Lat,Lon,Scale;
  int J,I;
  
  if(argc!=2) return(1);
  if(!(F=fopen(argv[1],"rb"))) return(2);

  Scale=10.0;

  while(fgets(S,sizeof(S)-1,F))
  {
    // Check lat/lon for correct syntax
    for(J=149;J<164;J++)
      if((J==155)||(J==163)) { if(!isalpha(S[J])) break; }
      else { if(!isdigit(S[J])) break; }

    if(J<164)
    {
      // Error in lat/lon
      S[164]='\0';
      cout<<"ERROR: Parsing point '"<<S+149<<"'"<<endl;
    }
    else  
    {
      // Parse lat/lon
      Lat=Digits(S+149,2)+Digits(S+151,2)/60.0+Digits(S+153,2)/3600.0;
      Lat=PI*Lat/180.0;
      if(S[155]=='S') Lat=-Lat;
      Lon=Digits(S+156,3)+Digits(S+159,2)/60.0+Digits(S+161,2)/3600.0;
      Lon=PI*Lon/180.0;
      if(S[163]=='W') Lon=-Lon;

      // Project lat/lon to map coordinates
      Lon=Scale*Lon;
      Lat=PI/4.0+Lat/2.0;
      Lat=Scale*log(tan(Lat));
      P=Point(Lon,Lat);

      // Add point to all trees
      T1.Add(P);
      T2.Add(P);
      T3.Add(P);
    }
  }

  cout<<"T1 now contains "<<T1.Cost(Rect())<<" points."<<endl;
  cout<<"T2 now contains "<<T2.Cost(Rect())<<" points."<<endl;
  cout<<"T3 now contains "<<T3.Cost(Rect())<<" points."<<endl;

  Box1=Point(-13.0987,7.27911);
  do
  {
    clock_t TM1,TM2;

    cout<<"T2 now contains "<<T2.Cost(Rect())<<" points. Next NN:"<<endl;
    TM1=clock();
    for(I=0,R=Q5.GetFirst(QC,&J);R;R=Q5.GetNext(QC,0,&J))
      if(!J)
      {
        P=Point(((Rect *)R)->X1(),((Rect *)R)->Y1());
        T2.Add(P);
        cout<<P<<" at "<<Box1.MinD(P)<<endl;
        I++;
      }
    TM2=clock();
    cout<<"Time: "<<(double)(TM2-TM1)/1000.0<<"ms"<<endl;
  }
  while(I);

  fclose(F);
  return(0);
}

